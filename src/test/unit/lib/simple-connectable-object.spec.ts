import {SimpleConnectableObject} from '../../../lib/simple-connectable-object';

describe('#SimpleConnectableObject', function () {
  function createInstance(): SimpleConnectableObject {
    return new SimpleConnectableObject();
  }

  describe('#_getStateNamesForReady', function () {
    it("Then must return expected value", function () {
      const instance = createInstance();

      expect(instance['_getStateNamesForReady']()).toEqual(['connected']);
    });
  }); // #_getStateNamesForReady
}); // #SimpleConnectableObject
