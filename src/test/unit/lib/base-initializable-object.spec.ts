import {BaseInitializableObject} from '../../../lib/base-initializable-object';
import {common as commonErrors} from 'zrim-errors';
import * as proxyLoggerLib from 'zrim-proxy-logger';
import {BaseObject} from "../../../lib/base-object";

describe('#BaseInitializableObject', function () {
  function createInstance(): BaseInitializableObject {
    return new BaseInitializableObject();
  }

  describe('#constructor', function () {
    it("Then must set new state", function () {
      // @ts-ignore
      spyOn(BaseInitializableObject.prototype, '_addActivatedStateName');

      const instance = new BaseInitializableObject();
      expect(instance['properties'].currentStateName).toBe('notInitialized');
      expect(BaseInitializableObject.prototype['_addActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(BaseInitializableObject.prototype['_addActivatedStateName']).toHaveBeenCalledWith('notInitialized');
    });
  }); // #constructor

  describe('#initialize', function () {
    it("Given cannot initialize Then must return error", async function () {
      const instance = createInstance();

      spyOn(instance, 'canInitialize').and.returnValue(false);
      // @ts-ignore
      spyOn(instance, '_handleInitialization').and.callFake(() => Promise.reject(new Error('Must not be called')));

      try {
        await instance.initialize({});
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toEqual(jasmine.any(commonErrors.IllegalStateError));
        expect(instance.canInitialize).toHaveBeenCalled();
        expect(instance['_handleInitialization']).not.toHaveBeenCalled();
      }
    });

    it("Given _handleInitialization to fail Then must fail", async function () {
      const instance = createInstance();

      spyOn(instance, 'canInitialize').and.returnValue(true);
      spyOn(instance, 'emit');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');

      const expectedError = new Error('Unit Test - Fake Error');
      // @ts-ignore
      spyOn(instance, '_handleInitialization').and.callFake(() => {
        expect(instance.currentStateName).toBe('initializing');
        return Promise.reject(expectedError);
      });

      Object.defineProperty(instance, 'currentStateName', {
        value: 'cnh',
        writable: true
      });

      const options = {
        oi: 10
      };
      try {
        await instance.initialize(options, 'po');
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toBe(expectedError);
        expect(instance.canInitialize).toHaveBeenCalled();
        expect(instance.currentStateName).toBe('cnh');
        expect(instance['_handleInitialization']).toHaveBeenCalledTimes(1);
        expect(instance['_handleInitialization']).toHaveBeenCalledWith(options, 'po', {
          logger: jasmine.any(Object)
        });

        expect(instance.emit).toHaveBeenCalledTimes(2);
        expect(instance.emit).toHaveBeenCalledWith('initializing', {_origin: instance});
        expect(instance.emit).toHaveBeenCalledWith('initializationFailed', {
          _origin: instance,
          error: expectedError
        });
        expect(instance['_addActivatedStateName']).not.toHaveBeenCalled();
        expect(instance['_removeActivatedStateName']).not.toHaveBeenCalled();
      }
    });

    it("Given _handleInitialization to success Then must success", async function () {
      const instance = createInstance();

      spyOn(instance, 'canInitialize').and.returnValue(true);
      spyOn(instance, 'emit');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');

      // @ts-ignore
      spyOn(instance, '_handleInitialization').and.callFake(() => {
        expect(instance.currentStateName).toBe('initializing');
        return Promise.resolve();
      });

      Object.defineProperty(instance, 'currentStateName', {
        value: 'cnh',
        writable: true
      });

      const options = {
        oi: 10
      };
      await instance.initialize(options, 'po');
      expect(instance.canInitialize).toHaveBeenCalled();
      expect(instance.currentStateName).toBe('initialized');
      expect(instance['_handleInitialization']).toHaveBeenCalledTimes(1);
      expect(instance['_handleInitialization']).toHaveBeenCalledWith(options, 'po', {
        logger: jasmine.any(Object)
      });

      expect(instance.emit).toHaveBeenCalledTimes(2);
      expect(instance.emit).toHaveBeenCalledWith('initializing', {_origin: instance});
      expect(instance.emit).toHaveBeenCalledWith('initializationSucceed', {_origin: instance});
      expect(instance['_addActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(instance['_addActivatedStateName']).toHaveBeenCalledWith('initialized');
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledWith('notInitialized');
    });
  }); // #initialize

  describe('#_handleInitialization', function () {
    it("Then must return success", async function () {
      const instance = createInstance();

      await instance['_handleInitialization']({}, {
        logger: new proxyLoggerLib.tests.mocks.SimpleGenericProxyLogger()
      });
      expect(true).toBeTruthy();
    });
  }); // #_handleInitialization

  describe('#canInitialize', function () {
    it("Given no activated Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(false);
      expect(instance.canInitialize()).toBe(false);
      expect(instance.isStateActivated).toHaveBeenCalledWith('notInitialized');
    });

    it("Given activated Then must return true", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(true);
      expect(instance.canInitialize()).toBe(true);
      expect(instance.isStateActivated).toHaveBeenCalledWith('notInitialized');
    });
  }); // #canInitialize

  describe('#isInitialized', function () {
    it("Given no activated Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(false);
      expect(instance.isInitialized()).toBe(false);
      expect(instance.isStateActivated).toHaveBeenCalledWith('initialized');
    });

    it("Given activated Then must return true", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(true);
      expect(instance.isInitialized()).toBe(true);
      expect(instance.isStateActivated).toHaveBeenCalledWith('initialized');
    });
  }); // #isInitialized

  describe('#finalize', function () {
    it("Given cannot finalize Then must return error", async function () {
      const instance = createInstance();

      spyOn(instance, 'canFinalize').and.returnValue(false);
      // @ts-ignore
      spyOn(instance, '_handleFinalization').and.callFake(() => Promise.reject(new Error('Must not be called')));

      try {
        await instance.finalize({});
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toEqual(jasmine.any(commonErrors.IllegalStateError));
        expect(instance.canFinalize).toHaveBeenCalled();
        expect(instance['_handleFinalization']).not.toHaveBeenCalled();
      }
    });

    it("Given _handleFinalization to fail Then must fail", async function () {
      const instance = createInstance();

      spyOn(instance, 'canFinalize').and.returnValue(true);
      spyOn(instance, 'emit');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');

      const expectedError = new Error('Unit Test - Fake Error');
      // @ts-ignore
      spyOn(instance, '_handleFinalization').and.callFake(() => {
        expect(instance.currentStateName).toBe('finalizing');
        return Promise.reject(expectedError);
      });

      Object.defineProperty(instance, 'currentStateName', {
        value: 'cnh',
        writable: true
      });

      const options = {
        oi: 10
      };
      try {
        await instance.finalize(options, 'po');
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toBe(expectedError);
        expect(instance.canFinalize).toHaveBeenCalled();
        expect(instance.currentStateName).toBe('cnh');
        expect(instance['_handleFinalization']).toHaveBeenCalledTimes(1);
        expect(instance['_handleFinalization']).toHaveBeenCalledWith(options, 'po', {
          logger: jasmine.any(Object)
        });

        expect(instance.emit).toHaveBeenCalledTimes(2);
        expect(instance.emit).toHaveBeenCalledWith('finalizing', {_origin: instance});
        expect(instance.emit).toHaveBeenCalledWith('finalizationFailed', {
          _origin: instance,
          error: expectedError
        });
        expect(instance['_addActivatedStateName']).toHaveBeenCalledTimes(1);
        expect(instance['_addActivatedStateName']).toHaveBeenCalledWith('initialized');
        expect(instance['_removeActivatedStateName']).toHaveBeenCalledTimes(1);
        expect(instance['_removeActivatedStateName']).toHaveBeenCalledWith('initialized');
      }
    });

    it("Given _handleFinalization to success Then must success", async function () {
      const instance = createInstance();

      spyOn(instance, 'canFinalize').and.returnValue(true);
      spyOn(instance, 'emit');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');

      // @ts-ignore
      spyOn(instance, '_handleFinalization').and.callFake(() => {
        expect(instance.currentStateName).toBe('finalizing');
        return Promise.resolve();
      });

      Object.defineProperty(instance, 'currentStateName', {
        value: 'cnh',
        writable: true
      });

      const options = {
        oi: 10
      };
      await instance.finalize(options, 'po');
      expect(instance.canFinalize).toHaveBeenCalled();
      expect(instance.currentStateName).toBe('notInitialized');
      expect(instance['_handleFinalization']).toHaveBeenCalledTimes(1);
      expect(instance['_handleFinalization']).toHaveBeenCalledWith(options, 'po', {
        logger: jasmine.any(Object)
      });

      expect(instance.emit).toHaveBeenCalledTimes(2);
      expect(instance.emit).toHaveBeenCalledWith('finalizing', {_origin: instance});
      expect(instance.emit).toHaveBeenCalledWith('finalizationSucceed', {_origin: instance});
      expect(instance['_addActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(instance['_addActivatedStateName']).toHaveBeenCalledWith('notInitialized');
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledWith('initialized');
    });
  }); // #finalize

  describe('#_handleFinalization', function () {
    it("Then must return success", async function () {
      const instance = createInstance();

      await instance['_handleFinalization']({
        logger: new proxyLoggerLib.tests.mocks.SimpleGenericProxyLogger()
      });
      expect(true).toBeTruthy();
    });
  }); // #_handleFinalization

  describe('#canFinalize', function () {
    it("Given no activated Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(false);
      expect(instance.canFinalize()).toBe(false);
      expect(instance.isStateActivated).toHaveBeenCalledWith('initialized');
    });

    it("Given activated Then must return true", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(true);
      expect(instance.canFinalize()).toBe(true);
      expect(instance.isStateActivated).toHaveBeenCalledWith('initialized');
    });
  }); // #canFinalize

  describe('#_acceptStateName', function () {
    it("Given not valid value Then must return false", function () {
      const instance = createInstance();

      spyOn(BaseInitializableObject, 'getStateNames').and.returnValue([]);
      expect(instance['_acceptStateName']('sls')).toBe(false);
      expect(BaseInitializableObject.getStateNames).toHaveBeenCalled();
    });

    it("Given valid value Then must return true", function () {
      const instance = createInstance();

      spyOn(BaseInitializableObject, 'getStateNames').and.returnValue(['dd', 'sls', 'po']);
      expect(instance['_acceptStateName']('sls')).toBe(true);
      expect(BaseInitializableObject.getStateNames).toHaveBeenCalled();
    });
  }); // #_acceptStateName

  describe('#getStateNames', function () {
    const _ = require('lodash');

    it("Then must return expected value", function () {
      expect(BaseInitializableObject.getStateNames()).toEqual(jasmine.arrayContaining(_.concat([
        'notInitialized',
        'initializing',
        'initialized',
        'finalizing'
      ], BaseObject.getStateNames())));
    });
  }); // #getStateNames

  describe('#getSignalNames', function () {
    const _ = require('lodash');

    it("Then must return expected value", function () {
      expect(BaseInitializableObject.getSignalNames()).toEqual(jasmine.arrayContaining(_.concat([
        'initializing',
        'initializationSucceed',
        'initializationFailed',
        'finalizing',
        'finalizationSucceed',
        'finalizationFailed'
      ], BaseObject.getSignalNames())));
    });
  }); // #getSignalNames
}); // #BaseInitializableObject

