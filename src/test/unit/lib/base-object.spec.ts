import {baseObject, BaseObject} from '../../../lib/base-object';
import {jasmineUtils} from "zrim-test-bootstrap/dist/lib/utils";
import {SimpleGenericProxyLogger} from "zrim-proxy-logger";
import * as utils from 'zrim-utils';

describe('#BaseObject', function () {
  function createInstance(): BaseObject {
    return new BaseObject();
  }

  describe('#construct', function () {
    const utils = require('zrim-utils');

    it("Then must create properties and call _initFromConstructor", function () {
      // @ts-ignore
      spyOn(BaseObject.prototype, '_initFromConstructor');
      spyOn(utils.classUtils, 'getClassName').and.returnValue('ikhjn');

      const proxyLoggerInstance = jasmineUtils.spyOnClass(SimpleGenericProxyLogger);
      // @ts-ignore
      spyOn(BaseObject.prototype, '_newProxyLogger').and.returnValue(proxyLoggerInstance);

      const args = [12, 'oi'];
      const instance = new BaseObject({} as baseObject.ConstructorOptions, ...args);
      expect(Reflect.getOwnPropertyDescriptor(instance, 'properties')).toEqual({
        value: {
          activatedStates: {},
          currentStateName: 'none',
          __className: 'ikhjn',
          ignoreSignals: false
        },
        writable: true,
        enumerable: false,
        configurable: false
      });
      expect(Reflect.getOwnPropertyDescriptor(instance, 'logger')).toEqual({
        value: proxyLoggerInstance,
        writable: true,
        enumerable: false,
        configurable: false
      });

      expect(BaseObject.prototype['_initFromConstructor']).toHaveBeenCalledTimes(1);
      expect(BaseObject.prototype['_initFromConstructor']).toHaveBeenCalledWith({}, ...args);

      expect(utils.classUtils.getClassName).toHaveBeenCalledTimes(1);
      expect(utils.classUtils.getClassName).toHaveBeenCalledWith(instance);
    });
  }); // #construct

  describe('#_initFromConstructor', function () {
    it("Given no object Then must do nothing", function () {
      const instance = {
        properties: {
          l: 10
        }
      };

      BaseObject.prototype['_initFromConstructor'].call(instance);
      expect(instance.properties).toEqual({
        l: 10
      });
    });

    it("Given options object without target Then must do nothing", function () {
      const instance = {
        properties: {
          l: 10
        }
      };

      BaseObject.prototype['_initFromConstructor'].call(instance, {});
      expect(instance.properties).toEqual({
        l: 10
      });
    });

    it("Given options object with target Then must set the target", function () {
      const instance = {
        properties: {
          l: 10
        },
        logger: jasmineUtils.spyOnClass(SimpleGenericProxyLogger)
      };

      const options = {
        targetLogger: jasmineUtils.spyOnClass(SimpleGenericProxyLogger)
      };
      BaseObject.prototype['_initFromConstructor'].call(instance, options);
      expect(instance.logger.target).toEqual(options.targetLogger);
    });
  }); // #_initFromConstructor

  describe('#__className', function () {
    describe('#get', function () {
      it("Then must return expected value", function () {
        const instance = createInstance();
        instance['properties'].__className = 'ikjs';
        expect(instance.__className).toBe('ikjs');
      });
    }); // #get
  }); // #__className

  describe('#ignoreSignals', function () {
    describe('#get', function () {
      it("Given false Then must return expected value", function () {
        const instance = createInstance();
        instance['properties'].ignoreSignals = false;
        expect(instance.ignoreSignals).toBe(false);
      });

      it("Given true Then must return expected value", function () {
        const instance = createInstance();
        instance['properties'].ignoreSignals = true;
        expect(instance.ignoreSignals).toBe(true);
      });
    }); // #get

    describe('#set', function () {
      it("Given invalid type value Then must do nothing", function () {
        const instance = createInstance();

        instance['properties'].ignoreSignals = undefined;
        // @ts-ignore
        spyOn(instance, '_emit');
        // @ts-ignore
        spyOn(instance, '_emitFlagChanged');

        instance.ignoreSignals = undefined;
        // @ts-ignore
        instance.ignoreSignals = 12;
        // @ts-ignore
        instance.ignoreSignals = 'a';
        // @ts-ignore
        instance.ignoreSignals = () => {};
        // @ts-ignore
        instance.ignoreSignals = {};
        expect(instance['_emit']).not.toHaveBeenCalled();
        expect(instance['_emitFlagChanged']).not.toHaveBeenCalled();
      });

      it("Given same value Then must do nothing", function () {
        const instance = createInstance();

        instance['properties'].ignoreSignals = false;
        // @ts-ignore
        spyOn(instance, '_emit');
        // @ts-ignore
        spyOn(instance, '_emitFlagChanged');
        instance.ignoreSignals = false;
        expect(instance['_emit']).not.toHaveBeenCalled();
        expect(instance['_emitFlagChanged']).not.toHaveBeenCalled();
      });

      it("Given new value Then must return change value and emit events", function () {
        const instance = createInstance();

        instance['properties'].ignoreSignals = false;
        // @ts-ignore
        spyOn(instance, '_emit');
        // @ts-ignore
        spyOn(instance, '_emitFlagChanged');
        instance.ignoreSignals = true;

        expect(instance['properties'].ignoreSignals).toBe(true);
        expect(instance['_emit']).toHaveBeenCalledTimes(1);
        expect(instance['_emit']).toHaveBeenCalledWith('ignoreSignalsChanged', {
          _origin: instance,
          previousValue: false,
          newValue: true
        });
        expect(instance['_emitFlagChanged']).toHaveBeenCalledTimes(1);
        expect(instance['_emitFlagChanged']).toHaveBeenCalledWith('ignoreSignals', true, false);
      });
    }); // #set
  }); // #ignoreSignals

  describe('#currentStateName', function () {
    describe('#get', function () {
      it("Given value Then must return expected value", function () {
        const instance = createInstance();

        instance['properties'].currentStateName = 's;j';
        expect(instance.currentStateName).toBe('s;j');
      });
    }); // #get

    describe('#set', function () {
      it("Given same value Then must do nothing", function () {
        const instance = createInstance();

        instance['properties'].currentStateName = 'ikj';
        // @ts-ignore
        spyOn(instance, '_acceptStateName');
        // @ts-ignore
        spyOn(instance, '_emitPropertyChanged');

        instance.currentStateName = 'ikj';
        expect(instance['_acceptStateName']).not.toHaveBeenCalled();
        expect(instance['_emitPropertyChanged']).not.toHaveBeenCalled();
      });

      it("Given different value but not accepted Then must do nothing", function () {
        const instance = createInstance();

        instance['properties'].currentStateName = 'ikj';
        // @ts-ignore
        spyOn(instance, '_acceptStateName').and.returnValue(false);
        // @ts-ignore
        spyOn(instance, '_emitPropertyChanged');

        instance.currentStateName = 'polk';
        expect(instance['_acceptStateName']).toHaveBeenCalledWith('polk');
        expect(instance['_emitPropertyChanged']).not.toHaveBeenCalled();
      });

      it("Given different value and accepted Then must set and emit events", function () {
        const instance = createInstance();

        instance['properties'].currentStateName = 'ikj';
        // @ts-ignore
        spyOn(instance, '_acceptStateName').and.returnValue(true);
        // @ts-ignore
        spyOn(instance, '_emitPropertyChanged');

        instance.currentStateName = 'polk';
        expect(instance['properties'].currentStateName).toBe('polk');
        expect(instance['_acceptStateName']).toHaveBeenCalledWith('polk');
        expect(instance['_emitPropertyChanged']).toHaveBeenCalledTimes(1);
        expect(instance['_emitPropertyChanged']).toHaveBeenCalledWith('currentStateName', 'polk', 'ikj');
      });
    }); // #set
  }); // #currentStateName

  describe('#_acceptStateName', function () {
    it("Given not valid value Then must return false", function () {
      const instance = createInstance();

      spyOn(BaseObject, 'getStateNames').and.returnValue([]);
      expect(instance['_acceptStateName']('sls')).toBe(false);
      expect(BaseObject.getStateNames).toHaveBeenCalled();
    });

    it("Given valid value Then must return true", function () {
      const instance = createInstance();

      spyOn(BaseObject, 'getStateNames').and.returnValue(['dd', 'sls', 'po']);
      expect(instance['_acceptStateName']('sls')).toBe(true);
      expect(BaseObject.getStateNames).toHaveBeenCalled();
    });
  }); // #_acceptStateName

  describe('#getStateNames', function () {
    it("Then must return expected value", function () {
      expect(BaseObject.getStateNames()).toEqual(jasmine.arrayContaining([
        'none',
        'ready'
      ]));
    });
  }); // #getStateNames

  describe('#getSignalNames', function () {
    it("Then must return expected value", function () {
      expect(BaseObject.getSignalNames()).toEqual(jasmine.arrayContaining([
        'ready',
        'currentStateChanged',
        'ignoreSignalsChanged',
        'flagChanged',
        'propertyChanged'
      ]));
    });
  }); // #getSignalNames

  describe('#_emit', function () {
    it("Then must call the emit", function () {
      const instance = createInstance();

      const EventEmitter = require('events').EventEmitter;
      spyOn(EventEmitter.prototype, 'emit').and.returnValue('as');
      expect(instance['_emit']('asc', 'pol', 'ikj')).toBe('as');
      expect(EventEmitter.prototype.emit).toHaveBeenCalledTimes(1);
      expect(EventEmitter.prototype.emit).toHaveBeenCalledWith('asc', 'pol', 'ikj');
    });
  }); // #_emit

  describe('#emit', function () {
    const EventEmitter = require('events').EventEmitter;

    it("Given signal not enabled Then must not call the emit", function () {
      const instance = createInstance();

      Object.defineProperty(instance, 'ignoreSignals', {value: true});

      spyOn(EventEmitter.prototype, 'emit').and.returnValue('as');
      instance.emit('asc', 'pol', 'ikj');
      expect(EventEmitter.prototype.emit).not.toHaveBeenCalled();
    });

    it("Given signal enabled Then must call the emit", function () {
      const instance = createInstance();

      Object.defineProperty(instance, 'ignoreSignals', {value: false});

      spyOn(EventEmitter.prototype, 'emit').and.returnValue('as');
      instance.emit('asc', 'pol', 'ikj');
      expect(EventEmitter.prototype.emit).toHaveBeenCalledTimes(1);
      expect(EventEmitter.prototype.emit).toHaveBeenCalledWith('asc', 'pol', 'ikj');
    });

    it("Given super.emit to fail Then must ignore error", function () {
      const instance = createInstance();

      Object.defineProperty(instance, 'ignoreSignals', {value: false});

      const error = new Error(`Unit Test - Fake Error`);
      // @ts-ignore
      spyOn(EventEmitter.prototype, 'emit').and.throwError(error);
      instance.emit('asc', 'pol', 'ikj');
      expect(EventEmitter.prototype.emit).toHaveBeenCalledTimes(1);
      expect(EventEmitter.prototype.emit).toHaveBeenCalledWith('asc', 'pol', 'ikj');
    });
  }); // #emit

  describe('#_emitPropertyChanged', function () {
    it("Given properties Then must call emit", function () {
      const instance = createInstance();

      spyOn(instance, 'emit');
      instance['_emitPropertyChanged']('ujd', 'pod', 'uj');
      expect(instance.emit).toHaveBeenCalledTimes(2);
      expect(instance.emit).toHaveBeenCalledWith('propertyChanged', {
        _origin: instance,
        name: 'ujd',
        newValue: 'pod',
        previousValue: 'uj'
      });
      expect(instance.emit).toHaveBeenCalledWith('ujdChanged', {
        _origin: instance,
        newValue: 'pod',
        previousValue: 'uj'
      });
    });
  }); // #_emitPropertyChanged

  describe('#_emitCurrentStateChanged', function () {
    it("Given properties Then must call emit", function () {
      const instance = createInstance();

      spyOn(instance, 'emit');
      instance['_emitCurrentStateChanged']('pod', 'uj');
      expect(instance.emit).toHaveBeenCalledTimes(1);
      expect(instance.emit).toHaveBeenCalledWith('currentStateChanged', {
        _origin: instance,
        newValue: 'pod',
        previousValue: 'uj'
      });
    });
  }); // #_emitCurrentStateChanged

  describe('#_emitFlagChanged', function () {
    it("Given properties Then must call emit", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_emit');
      instance['_emitFlagChanged']('ujd', 'pod', 'uj');
      expect(instance['_emit']).toHaveBeenCalledTimes(1);
      expect(instance['_emit']).toHaveBeenCalledWith('flagChanged', {
        _origin: instance,
        name: 'ujd',
        newValue: 'pod',
        previousValue: 'uj'
      });
    });
  }); // #_emitFlagChanged

  describe('#isStateActivated', function () {
    it("Given not active Then must return false", function () {
      const instance = createInstance();

      instance['properties'].activatedStates = {
        l: {
          activatedAt: new Date(8877)
        }
      };
      expect(instance.isStateActivated('c')).toBe(false);
    });

    it("Given active Then must return true", function () {
      const instance = createInstance();

      instance['properties'].activatedStates = {
        l: {
          activatedAt: new Date(8877)
        },
        c: {
          activatedAt: new Date(8877)
        }
      };
      expect(instance.isStateActivated('c')).toBe(true);
    });
  }); // #isStateActivated

  describe('#isReady', function () {
    it("Given no active Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(false);
      expect(instance.isReady()).toBe(false);
      expect(instance.isStateActivated).toHaveBeenCalledTimes(1);
      expect(instance.isStateActivated).toHaveBeenCalledWith('ready');
    });

    it("Given active Then must return true", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(true);
      expect(instance.isReady()).toBe(true);
      expect(instance.isStateActivated).toHaveBeenCalledTimes(1);
      expect(instance.isStateActivated).toHaveBeenCalledWith('ready');
    });
  }); // #isReady

  describe('#_addActivatedStateName', function () {
    it("Given no string Then must throw error", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_emit');
      // @ts-ignore
      spyOn(instance, '_updateReadyState');

      [{}, 12, false, null, undefined, ''].forEach(arg => {
        // @ts-ignore
        expect(() => instance['_addActivatedStateName'](arg)).toThrow(jasmine.any(TypeError));
      });

      expect(instance['_emit']).not.toHaveBeenCalled();
      expect(instance['_updateReadyState']).not.toHaveBeenCalled();
    });

    it("Given valid string Then must add it", function () {
      const instance = createInstance();

      spyOn(Date, 'now').and.returnValue(666);
      // @ts-ignore
      spyOn(instance, '_emit');
      // @ts-ignore
      spyOn(instance, '_updateReadyState');

      instance['_addActivatedStateName']('tgj');
      expect(instance['properties'].activatedStates.tgj).toEqual({
        activatedAt: new Date(666)
      });
      expect(instance['_emit']).toHaveBeenCalledTimes(1);
      expect(instance['_emit']).toHaveBeenCalledWith('stateActivated', {
        _origin: instance,
        stateName: 'tgj'
      });
      expect(instance['_updateReadyState']).toHaveBeenCalled();
    });
  }); // #_addActivatedStateName

  describe('#_removeActivatedStateName', function () {
    it("Given no string Then must throw error", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_emit');
      // @ts-ignore
      spyOn(instance, '_updateReadyState');

      [{}, 12, false, null, undefined, ''].forEach(arg => {
        // @ts-ignore
        expect(() => instance['_removeActivatedStateName'](arg)).toThrow(jasmine.any(TypeError));
      });

      expect(instance['_emit']).not.toHaveBeenCalled();
      expect(instance['_updateReadyState']).not.toHaveBeenCalled();
    });

    it("Given valid string Then must delete it", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_emit');
      // @ts-ignore
      spyOn(instance, '_updateReadyState');

      instance['properties'].activatedStates.olk = {
        activatedAt: new Date(5566)
      };

      instance['_removeActivatedStateName']('olk');
      expect(Reflect.getOwnPropertyDescriptor(instance['properties'].activatedStates, 'olk')).toBeUndefined();
      expect(instance['_emit']).toHaveBeenCalledWith('stateDeactivated', {
        _origin: instance,
        stateName: 'olk'
      });
      expect(instance['_updateReadyState']).toHaveBeenCalled();
    });
  }); // #_removeActivatedStateName

  describe('#getActivatedStateNames', function () {
    it("Then must return expected value", function () {
      const instance = createInstance();

      instance['properties'].activatedStates = {
        a: { activatedAt: new Date(5588) },
        y: { activatedAt: new Date(5588) },
        cv: {activatedAt: new Date(5588) }
      };
      expect(instance.getActivatedStateNames()).toEqual(jasmine.arrayContaining(['a', 'y', 'cv']));
    });
  }); // #getActivatedStateNames

  describe('#_newProxyLogger', function () {
    it("Then must create a logger", function () {
      const instance = createInstance();
      const proxyLoggerLib = require('zrim-proxy-logger');

      instance['properties'].__className = 'ikhjn';

      const proxyLoggerInstance = {
        o: 78
      };
      spyOn(proxyLoggerLib, 'SimpleGenericProxyLogger').and.returnValue(proxyLoggerInstance);

      const targetLogger = {
        b: 10
      };
      spyOn(proxyLoggerLib.defaultLoggerManagerHandler, 'getLogger').and.returnValue(targetLogger);

      // @ts-ignore
      expect(instance['_newProxyLogger']('a', 'p')).toBe(proxyLoggerInstance);

      expect(proxyLoggerLib.SimpleGenericProxyLogger).toHaveBeenCalledWith({
        target: targetLogger,
        prefixes: ['ikhjn']
      });
      expect(proxyLoggerLib.defaultLoggerManagerHandler.getLogger).toHaveBeenCalledTimes(1);
      expect(proxyLoggerLib.defaultLoggerManagerHandler.getLogger).toHaveBeenCalledWith('ikhjn');
    });
  }); // #_newProxyLogger

  describe('#_getStateNamesForReady', function () {
    it("Then must return undefined", function () {
      const instance = createInstance();

      expect(instance['_getStateNamesForReady']()).toBeUndefined();
    });
  }); // #_getStateNamesForReady

  describe('#_updateReadyState', function () {
    it("Given undefined states for ready Then must do nothing", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_getStateNamesForReady');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');
      spyOn(instance, 'isStateActivated');
      spyOn(instance, 'isReady');

      instance['_updateReadyState']();
      expect(instance['_getStateNamesForReady']).toHaveBeenCalled();
      expect(instance['_addActivatedStateName']).not.toHaveBeenCalled();
      expect(instance['_removeActivatedStateName']).not.toHaveBeenCalled();
      expect(instance['isStateActivated']).not.toHaveBeenCalled();
      expect(instance['isReady']).not.toHaveBeenCalled();
    });

    it("Given no states for ready and not ready Then must pass to ready", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_getStateNamesForReady').and.returnValue([]);
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');
      spyOn(instance, 'isStateActivated');
      spyOn(instance, 'isReady').and.returnValue(false);

      instance['_updateReadyState']();
      expect(instance['_getStateNamesForReady']).toHaveBeenCalled();
      expect(instance['isReady']).toHaveBeenCalled();
      expect(instance['_addActivatedStateName']).toHaveBeenCalledWith('ready');
      expect(instance['_removeActivatedStateName']).not.toHaveBeenCalled();
      expect(instance['isStateActivated']).not.toHaveBeenCalled();
    });

    it("Given no states for ready and ready Then must do nothing", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_getStateNamesForReady').and.returnValue([]);
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');
      spyOn(instance, 'isStateActivated');
      spyOn(instance, 'isReady').and.returnValue(true);

      instance['_updateReadyState']();
      expect(instance['_getStateNamesForReady']).toHaveBeenCalled();
      expect(instance['isReady']).toHaveBeenCalled();
      expect(instance['_addActivatedStateName']).not.toHaveBeenCalled();
      expect(instance['_removeActivatedStateName']).not.toHaveBeenCalled();
      expect(instance['isStateActivated']).not.toHaveBeenCalled();
    });

    it("Given states for ready and ready and state not active Then must remove state", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_getStateNamesForReady').and.returnValue(['not-found']);
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');
      spyOn(instance, 'isStateActivated').and.returnValue(false);
      spyOn(instance, 'isReady').and.returnValue(true);

      instance['_updateReadyState']();
      expect(instance['_getStateNamesForReady']).toHaveBeenCalled();
      expect(instance['isReady']).toHaveBeenCalled();
      expect(instance['_addActivatedStateName']).not.toHaveBeenCalled();
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledWith('ready');
      expect(instance['isStateActivated']).toHaveBeenCalledWith('not-found');
    });

    it("Given states for ready and not ready and state active Then must add state", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_getStateNamesForReady').and.returnValue(['found-1', 'found-2']);
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');
      spyOn(instance, 'isStateActivated').and.returnValue(true);
      spyOn(instance, 'isReady').and.returnValue(false);

      instance['_updateReadyState']();
      expect(instance['_getStateNamesForReady']).toHaveBeenCalled();
      expect(instance['isReady']).toHaveBeenCalled();
      expect(instance['_addActivatedStateName']).toHaveBeenCalledWith('ready');
      expect(instance['_removeActivatedStateName']).not.toHaveBeenCalled();
      expect(instance['isStateActivated']).toHaveBeenCalledWith('found-1');
      expect(instance['isStateActivated']).toHaveBeenCalledWith('found-2');
    });
  }); // #_updateReadyState
}); // #BaseObject

