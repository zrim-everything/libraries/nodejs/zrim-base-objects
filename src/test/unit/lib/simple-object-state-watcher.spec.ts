import {SimpleObjectStateWatcher} from '../../../lib/simple-object-state-watcher';
import {SimpleRunnableObject} from "../../../lib/simple-runnable-object";
import {jasmineUtils} from "zrim-test-bootstrap/dist/lib/utils";
import {BaseObject} from "../../../lib/base-object";
import {BaseRunnableObject} from "../../../lib/base-runnable-object";

describe('#SimpleObjectStateWatcher', function () {
  function createInstance(): SimpleObjectStateWatcher {
    return new SimpleObjectStateWatcher();
  }

  describe('#construct', function () {
    it('Then must set properties', function () {
      const instance = new SimpleObjectStateWatcher();

      expect(instance['properties'].waitingStateName).toBe('ready');
      expect(instance['properties'].synchronized).toBe(false);
      expect(instance['properties'].objectDescriptions).toEqual([]);
    });
  }); // #construct

  describe('#initialize', function () {
    it("Then must call super", async function () {
      const instance = createInstance();

      spyOn(SimpleRunnableObject.prototype, 'initialize').and.callFake(() => Promise.resolve());

      const options = {
        objects: [],
        waitingStateName: 'aaacc'
      };
      await instance.initialize(options, 'po');
      expect(SimpleRunnableObject.prototype['initialize']).toHaveBeenCalledWith(options, 'po');
    });
  }); // #initialize

  describe('#_handleInitialization', function () {
    it("Given valid options Then must register objects", async function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(SimpleRunnableObject.prototype, '_handleInitialization').and.callFake(() => Promise.resolve());

      const EventEmitter = require('events').EventEmitter;

      const options = {
        objects: [new EventEmitter(), new EventEmitter()],
        waitingStateName: 'aaacc'
      };
      await instance['_handleInitialization'](options, 'po');
      expect(instance['properties'].waitingStateName).toBe('aaacc');
      expect(instance['properties'].objectDescriptions).toEqual([{
        object: options.objects[0],
        activatedStateNames: [],
        onStateActivated: jasmine.any(Function),
        onStateDeactivated: jasmine.any(Function)
      }, {
        object: options.objects[1],
        activatedStateNames: [],
        onStateActivated: jasmine.any(Function),
        onStateDeactivated: jasmine.any(Function)
      }]);
      expect(SimpleRunnableObject.prototype['_handleInitialization']).toHaveBeenCalledWith(options, 'po');
    });
  }); // #_handleInitialization

  describe('#isSynchronized', function () {
    it("Given synchronized=false Then must return false", function () {
      const instance = createInstance();

      instance['properties'].synchronized = false;
      expect(instance.isSynchronized()).toBe(false);
    });

    it("Given synchronized=true Then must return true", function () {
      const instance = createInstance();

      instance['properties'].synchronized = true;
      expect(instance.isSynchronized()).toBe(true);
    });
  }); // #isSynchronized

  describe('#_onStateActivated', function () {
    it("Given no object for the description Then must do nothing", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_updateSynchronizedState');

      const event = {
        _origin: jasmineUtils.spyOnClass(BaseObject),
        stateName: 'ah'
      };
      instance['_onStateActivated'](event);
      expect(instance['_updateSynchronizedState']).not.toHaveBeenCalled();
    });

    it("Given object for the description Then must save the state and call update", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_updateSynchronizedState');

      const origin = jasmineUtils.spyOnClass(BaseObject);
      origin['p'] = 125;

      instance['properties'].objectDescriptions = [{
        object: jasmineUtils.spyOnClass(BaseObject),
        activatedStateNames: [],
        onStateActivated: () => {},
        onStateDeactivated: () => {}
      }, {
        object: origin,
        activatedStateNames: ['rf'],
        onStateActivated: () => {},
        onStateDeactivated: () => {}
      }];

      const event = {
        _origin: origin,
        stateName: 'ah'
      };
      instance['_onStateActivated'](event);
      expect(instance['_updateSynchronizedState']).toHaveBeenCalledTimes(1);
      expect(instance['properties'].objectDescriptions).toEqual([{
        object: instance['properties'].objectDescriptions[0].object,
        activatedStateNames: [],
        onStateActivated: instance['properties'].objectDescriptions[0].onStateActivated,
        onStateDeactivated: instance['properties'].objectDescriptions[0].onStateDeactivated
      }, {
        object: origin,
        activatedStateNames: ['rf', 'ah'],
        onStateActivated: instance['properties'].objectDescriptions[1].onStateActivated,
        onStateDeactivated: instance['properties'].objectDescriptions[1].onStateDeactivated
      }]);
    });
  }); // #_onStateActivated

  describe('#_onStateDeactivated', function () {
    it("Given no object for the description Then must do nothing", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_updateSynchronizedState');

      const event = {
        _origin: jasmineUtils.spyOnClass(BaseObject),
        stateName: 'ah'
      };
      instance['_onStateDeactivated'](event);
      expect(instance['_updateSynchronizedState']).not.toHaveBeenCalled();
    });

    it("Given object for the description Then must save the state and call update", function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_updateSynchronizedState');

      const origin = jasmineUtils.spyOnClass(BaseObject);
      origin['p'] = 125;

      instance['properties'].objectDescriptions = [{
        object: jasmineUtils.spyOnClass(BaseObject),
        activatedStateNames: [],
        onStateActivated: () => {},
        onStateDeactivated: () => {}
      }, {
        object: origin,
        activatedStateNames: ['rf', 'po', 'pi', 'po'],
        onStateActivated: () => {},
        onStateDeactivated: () => {}
      }];

      const event = {
        _origin: origin,
        stateName: 'po'
      };
      instance['_onStateDeactivated'](event);
      expect(instance['_updateSynchronizedState']).toHaveBeenCalledTimes(1);
      expect(instance['properties'].objectDescriptions).toEqual([{
        object: instance['properties'].objectDescriptions[0].object,
        activatedStateNames: [],
        onStateActivated: instance['properties'].objectDescriptions[0].onStateActivated,
        onStateDeactivated: instance['properties'].objectDescriptions[0].onStateDeactivated
      }, {
        object: origin,
        activatedStateNames: ['rf', 'pi'],
        onStateActivated: instance['properties'].objectDescriptions[1].onStateActivated,
        onStateDeactivated: instance['properties'].objectDescriptions[1].onStateDeactivated
      }]);
    });
  }); // #_onStateDeactivated

  describe('#_handleStart', function () {
    it("Given objects Then must add listeners", async function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_updateSynchronizedState');

      instance['properties'].objectDescriptions = [{
        object: jasmineUtils.spyOnClass(BaseObject),
        activatedStateNames: [],
        onStateActivated: () => {},
        onStateDeactivated: () => {}
      }, {
        object: jasmineUtils.spyOnClass(BaseObject),
        activatedStateNames: [],
        onStateActivated: () => {},
        onStateDeactivated: () => {}
      }];
      instance['properties'].objectDescriptions.forEach(item => {
        // @ts-ignore
        item.object.getActivatedStateNames.and.returnValue(['10', 'po']);
      });

      await instance['_handleStart']();
      expect(instance['_updateSynchronizedState']).toHaveBeenCalledTimes(1);
      instance['properties'].objectDescriptions.forEach(item => {
        expect(item.object.addListener).toHaveBeenCalledTimes(2);
        expect(item.object.addListener).toHaveBeenCalledWith('stateActivated', item.onStateActivated);
        expect(item.object.addListener).toHaveBeenCalledWith('stateDeactivated', item.onStateDeactivated);
        expect(item.activatedStateNames).toEqual(['10', 'po']);
      });
    });
  }); // #_handleStart

  describe('#_handleStop', function () {
    it("Given objects Then must remove listener", async function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_updateSynchronizedState');
      instance['properties'].objectDescriptions = [{
        object: jasmineUtils.spyOnClass(BaseObject),
        activatedStateNames: [],
        onStateActivated: () => {},
        onStateDeactivated: () => {}
      }, {
        object: jasmineUtils.spyOnClass(BaseObject),
        activatedStateNames: [],
        onStateActivated: () => {},
        onStateDeactivated: () => {}
      }];

      await instance['_handleStop']();
      expect(instance['_updateSynchronizedState']).not.toHaveBeenCalled();
      instance['properties'].objectDescriptions.forEach(item => {
        expect(item.object.removeListener).toHaveBeenCalledTimes(2);
        expect(item.object.removeListener).toHaveBeenCalledWith('stateActivated', item.onStateActivated);
        expect(item.object.removeListener).toHaveBeenCalledWith('stateDeactivated', item.onStateDeactivated);
      });
    });
  }); // #_handleStop

  describe('#_handleResume', function () {
    it("Then must call _handleStart", async function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_handleStart').and.callFake(() => Promise.resolve());

      await instance['_handleResume']('a', 'b');
      expect(instance['_handleStart']).toHaveBeenCalledTimes(1);
      expect(instance['_handleStart']).toHaveBeenCalledWith('a', 'b');
    });
  }); // #_handleResume

  describe('#_handlePause', function () {
    it("Then must call _handleStop", async function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_handleStop').and.callFake(() => Promise.resolve());

      await instance['_handlePause']('a', 'b');
      expect(instance['_handleStop']).toHaveBeenCalledTimes(1);
      expect(instance['_handleStop']).toHaveBeenCalledWith('a', 'b');
    });
  }); // #_handlePause

  describe('#_updateSynchronizedState', function () {
    it("Given same synchronization state Then must do nothing", function () {
      const instance = createInstance();

      spyOn(instance, 'emit');


      instance['properties'].objectDescriptions = [{
        object: jasmineUtils.spyOnClass(BaseObject),
        activatedStateNames: ['a', 'p'],
        onStateActivated: () => {},
        onStateDeactivated: () => {}
      }, {
        object: jasmineUtils.spyOnClass(BaseObject),
        activatedStateNames: ['y', 'p', 't'],
        onStateActivated: () => {},
        onStateDeactivated: () => {}
      }];

      instance['properties'].waitingStateName = 'p';
      instance['properties'].synchronized = true;

      instance['_updateSynchronizedState']();
      expect(instance['properties'].synchronized).toBe(true);
      expect(instance.emit).not.toHaveBeenCalled();
    });

    it("Given different synchronization state and not synchronized Then must emit desynchronized", function () {
      const instance = createInstance();

      spyOn(instance, 'emit');

      instance['properties'].objectDescriptions = [{
        object: jasmineUtils.spyOnClass(BaseObject),
        activatedStateNames: ['a', 'p'],
        onStateActivated: () => {},
        onStateDeactivated: () => {}
      }, {
        object: jasmineUtils.spyOnClass(BaseObject),
        activatedStateNames: ['y', 'o', 't'],
        onStateActivated: () => {},
        onStateDeactivated: () => {}
      }];

      instance['properties'].waitingStateName = 'p';
      instance['properties'].synchronized = true;

      instance['_updateSynchronizedState']();
      expect(instance['properties'].synchronized).toBe(false);
      expect(instance.emit).toHaveBeenCalledTimes(1);
      expect(instance.emit).toHaveBeenCalledWith('desynchronized', {
        _origin: instance
      });
    });

    it("Given same synchronization state and synchronized Then must emit synchronized", function () {
      const instance = createInstance();

      spyOn(instance, 'emit');

      instance['properties'].objectDescriptions = [{
        object: jasmineUtils.spyOnClass(BaseObject),
        activatedStateNames: ['a', 'p'],
        onStateActivated: () => {},
        onStateDeactivated: () => {}
      }, {
        object: jasmineUtils.spyOnClass(BaseObject),
        activatedStateNames: ['y', 'p', 't'],
        onStateActivated: () => {},
        onStateDeactivated: () => {}
      }];

      instance['properties'].waitingStateName = 'p';
      instance['properties'].synchronized = false;

      instance['_updateSynchronizedState']();
      expect(instance['properties'].synchronized).toBe(true);
      expect(instance.emit).toHaveBeenCalledTimes(1);
      expect(instance.emit).toHaveBeenCalledWith('synchronized', {
        _origin: instance
      });
    });

    it("Given no objects Then must emit synchronized", function () {
      const instance = createInstance();

      spyOn(instance, 'emit');

      instance['properties'].objectDescriptions = [];
      instance['properties'].waitingStateName = 'p';
      instance['properties'].synchronized = false;

      instance['_updateSynchronizedState']();
      expect(instance['properties'].synchronized).toBe(true);
      expect(instance.emit).toHaveBeenCalledTimes(1);
      expect(instance.emit).toHaveBeenCalledWith('synchronized', {
        _origin: instance
      });
    });
  }); // #_updateSynchronizedState

  describe('#_acceptStateName', function () {
    it("Given not valid value Then must return false", function () {
      const instance = createInstance();

      spyOn(SimpleObjectStateWatcher, 'getStateNames').and.returnValue([]);
      expect(instance['_acceptStateName']('sls')).toBe(false);
      expect(SimpleObjectStateWatcher.getStateNames).toHaveBeenCalled();
    });

    it("Given valid value Then must return true", function () {
      const instance = createInstance();

      spyOn(SimpleObjectStateWatcher, 'getStateNames').and.returnValue(['dd', 'sls', 'po']);
      expect(instance['_acceptStateName']('sls')).toBe(true);
      expect(SimpleObjectStateWatcher.getStateNames).toHaveBeenCalled();
    });
  }); // #_acceptStateName

  describe('#getSignalNames', function () {
    const _ = require('lodash');

    it("Then must return expected value", function () {
      expect(SimpleObjectStateWatcher.getSignalNames()).toEqual(jasmine.arrayContaining(_.concat([
        'synchronized',
        'desynchronized'
      ], BaseRunnableObject.getSignalNames())));
    });
  }); // #getSignalNames
}); // #SimpleObjectStateWatcher
