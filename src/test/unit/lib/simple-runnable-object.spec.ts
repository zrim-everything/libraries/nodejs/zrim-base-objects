import {SimpleRunnableObject} from '../../../lib/simple-runnable-object';

describe('#SimpleRunnableObject', function () {
  function createInstance(): SimpleRunnableObject {
    return new SimpleRunnableObject();
  }

  describe('#_getStateNamesForReady', function () {
    it("Then must return expected value", function () {
      const instance = createInstance();

      expect(instance['_getStateNamesForReady']()).toEqual(['initialized']);
    });
  }); // #_getStateNamesForReady
}); // #SimpleConnectableObject
