import {SimpleLoadableObject} from '../../../lib/simple-loadable-object';

describe('#SimpleLoadableObject', function () {
  function createInstance(): SimpleLoadableObject {
    return new SimpleLoadableObject();
  }

  describe('#_getStateNamesForReady', function () {
    it("Then must return expected value", function () {
      const instance = createInstance();

      expect(instance['_getStateNamesForReady']()).toEqual(['loaded']);
    });
  }); // #_getStateNamesForReady
}); // #SimpleConnectableObject
