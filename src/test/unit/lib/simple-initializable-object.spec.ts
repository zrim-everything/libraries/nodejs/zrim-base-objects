import {SimpleInitializableObject} from '../../../lib/simple-initializable-object';

describe('#SimpleInitializableObject', function () {
  function createInstance(): SimpleInitializableObject {
    return new SimpleInitializableObject();
  }

  describe('#_getStateNamesForReady', function () {
    it("Then must return expected value", function () {
      const instance = createInstance();

      expect(instance['_getStateNamesForReady']()).toEqual(['initialized']);
    });
  }); // #_getStateNamesForReady
}); // #SimpleConnectableObject
