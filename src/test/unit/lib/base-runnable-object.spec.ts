import {BaseRunnableObject} from '../../../lib/base-runnable-object';
import {common as commonErrors} from 'zrim-errors';
import * as proxyLoggerLib from 'zrim-proxy-logger';
import {BaseInitializableObject} from "../../../lib/base-initializable-object";


describe('#BaseRunnableObject', function () {
  function createInstance(): BaseRunnableObject {
    return new BaseRunnableObject();
  }

  describe('#start', function () {
    it("Given cannot start Then must return error", async function () {
      const instance = createInstance();

      spyOn(instance, 'canStart').and.returnValue(false);
      // @ts-ignore
      spyOn(instance, '_handleStart').and.callFake(() => Promise.reject(new Error('Must not be called')));

      try {
        await instance.start({});
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toEqual(jasmine.any(commonErrors.IllegalStateError));
        expect(instance.canStart).toHaveBeenCalled();
        expect(instance['_handleStart']).not.toHaveBeenCalled();
      }
    });

    it("Given _handleStart to fail Then must fail", async function () {
      const instance = createInstance();

      spyOn(instance, 'canStart').and.returnValue(true);
      spyOn(instance, 'emit');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');

      const expectedError = new Error('Unit Test - Fake Error');
      // @ts-ignore
      spyOn(instance, '_handleStart').and.callFake(() => {
        expect(instance.currentStateName).toBe('starting');
        return Promise.reject(expectedError);
      });

      Object.defineProperty(instance, 'currentStateName', {
        value: 'cnh',
        writable: true
      });

      const options = {
        oi: 10
      };
      try {
        await instance.start(options, 'po');
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toBe(expectedError);
        expect(instance.canStart).toHaveBeenCalled();
        expect(instance.currentStateName).toBe('cnh');
        expect(instance['_handleStart']).toHaveBeenCalledTimes(1);
        expect(instance['_handleStart']).toHaveBeenCalledWith(options, 'po', {
          logger: jasmine.any(Object)
        });

        expect(instance.emit).toHaveBeenCalledTimes(2);
        expect(instance.emit).toHaveBeenCalledWith('starting', {_origin: instance});
        expect(instance.emit).toHaveBeenCalledWith('startFailed', {
          _origin: instance,
          error: expectedError
        });
        expect(instance['_addActivatedStateName']).not.toHaveBeenCalled();
        expect(instance['_removeActivatedStateName']).not.toHaveBeenCalled();
      }
    });

    it("Given _handleStart to success Then must success", async function () {
      const instance = createInstance();

      spyOn(instance, 'canStart').and.returnValue(true);
      spyOn(instance, 'emit');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');

      // @ts-ignore
      spyOn(instance, '_handleStart').and.callFake(() => {
        expect(instance.currentStateName).toBe('starting');
        return Promise.resolve();
      });

      Object.defineProperty(instance, 'currentStateName', {
        value: 'cnh',
        writable: true
      });

      const options = {
        oi: 10
      };
      await instance.start(options, 'po');
      expect(instance.canStart).toHaveBeenCalled();
      expect(instance.currentStateName).toBe('running');
      expect(instance['_handleStart']).toHaveBeenCalledTimes(1);
      expect(instance['_handleStart']).toHaveBeenCalledWith(options, 'po', {
        logger: jasmine.any(Object)
      });

      expect(instance.emit).toHaveBeenCalledTimes(2);
      expect(instance.emit).toHaveBeenCalledWith('starting', {_origin: instance});
      expect(instance.emit).toHaveBeenCalledWith('started', {_origin: instance});
      expect(instance['_addActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(instance['_addActivatedStateName']).toHaveBeenCalledWith('running');
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledWith('finished');
    });
  }); // #start

  describe('#_handleStart', function () {
    it("Then must return success", async function () {
      const instance = createInstance();

      await instance['_handleStart']({
        logger: new proxyLoggerLib.tests.mocks.SimpleGenericProxyLogger()
      });
      expect(true).toBeTruthy();
    });
  }); // #_handleStart

  describe('#canStart', function () {
    it("Given neither initialized,finished Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(false);
      expect(instance.canStart()).toBe(false);
      expect(instance.isStateActivated).toHaveBeenCalledTimes(2);
      ['initialized', 'finished'].forEach(item => {
        expect(instance.isStateActivated).toHaveBeenCalledWith(item);
      });
    });

    it("Given initialized and pausing Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValues(true, true);
      expect(instance.canStart()).toBe(false);
      expect(instance.isStateActivated).toHaveBeenCalledTimes(2);
      ['initialized', 'pausing'].forEach(item => {
        expect(instance.isStateActivated).toHaveBeenCalledWith(item);
      });
    });

    it("Given finished and pausing Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValues(false, true, true);
      expect(instance.canStart()).toBe(false);
      expect(instance.isStateActivated).toHaveBeenCalledTimes(3);
      ['initialized', 'finished', 'pausing'].forEach(item => {
        expect(instance.isStateActivated).toHaveBeenCalledWith(item);
      });
    });

    it("Given finished nothing else Then must return true", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValues(false, true);
      expect(instance.canStart()).toBe(true);
      expect(instance.isStateActivated).toHaveBeenCalledTimes(6);
      ['initialized', 'finished', 'pausing', 'paused', 'resuming', 'running'].forEach(item => {
        expect(instance.isStateActivated).toHaveBeenCalledWith(item);
      });
    });

    it("Given initialized nothing else Then must return true", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValues(true);
      expect(instance.canStart()).toBe(true);
      expect(instance.isStateActivated).toHaveBeenCalledTimes(5);
      ['initialized', 'pausing', 'paused', 'resuming', 'running'].forEach(item => {
        expect(instance.isStateActivated).toHaveBeenCalledWith(item);
      });
    });
  }); // #canInitialize

  describe('#isRunning', function () {
    it("Given no activated Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(false);
      expect(instance.isRunning()).toBe(false);
      expect(instance.isStateActivated).toHaveBeenCalledWith('running');
    });

    it("Given activated Then must return true", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(true);
      expect(instance.isRunning()).toBe(true);
      expect(instance.isStateActivated).toHaveBeenCalledWith('running');
    });
  }); // #isRunning

  describe('#isPaused', function () {
    it("Given no activated Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(false);
      expect(instance.isPaused()).toBe(false);
      expect(instance.isStateActivated).toHaveBeenCalledWith('paused');
    });

    it("Given activated Then must return true", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(true);
      expect(instance.isPaused()).toBe(true);
      expect(instance.isStateActivated).toHaveBeenCalledWith('paused');
    });
  }); // #isPaused

  describe('#stop', function () {
    it("Given cannot stop Then must return error", async function () {
      const instance = createInstance();

      spyOn(instance, 'canStop').and.returnValue(false);
      // @ts-ignore
      spyOn(instance, '_handleStop').and.callFake(() => Promise.reject(new Error('Must not be called')));

      try {
        await instance.stop({});
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toEqual(jasmine.any(commonErrors.IllegalStateError));
        expect(instance.canStop).toHaveBeenCalled();
        expect(instance['_handleStop']).not.toHaveBeenCalled();
      }
    });

    it("Given _handleStop to fail Then must fail", async function () {
      const instance = createInstance();

      spyOn(instance, 'canStop').and.returnValue(true);
      spyOn(instance, 'emit');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');

      const expectedError = new Error('Unit Test - Fake Error');
      // @ts-ignore
      spyOn(instance, '_handleStop').and.callFake(() => {
        expect(instance.currentStateName).toBe('stopping');
        return Promise.reject(expectedError);
      });

      Object.defineProperty(instance, 'currentStateName', {
        value: 'cnh',
        writable: true
      });

      const options = {
        oi: 10
      };
      try {
        await instance.stop(options, 'po');
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toBe(expectedError);
        expect(instance.canStop).toHaveBeenCalled();
        expect(instance.currentStateName).toBe('cnh');
        expect(instance['_handleStop']).toHaveBeenCalledTimes(1);
        expect(instance['_handleStop']).toHaveBeenCalledWith(options, 'po', {
          logger: jasmine.any(Object)
        });

        expect(instance.emit).toHaveBeenCalledTimes(2);
        expect(instance.emit).toHaveBeenCalledWith('stopping', {_origin: instance});
        expect(instance.emit).toHaveBeenCalledWith('stopFailed', {
          _origin: instance,
          error: expectedError
        });
        expect(instance['_addActivatedStateName']).not.toHaveBeenCalled();
        expect(instance['_removeActivatedStateName']).not.toHaveBeenCalled();
      }
    });

    it("Given _handleStop to success Then must success", async function () {
      const instance = createInstance();

      spyOn(instance, 'canStop').and.returnValue(true);
      spyOn(instance, 'emit');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');

      // @ts-ignore
      spyOn(instance, '_handleStop').and.callFake(() => {
        expect(instance.currentStateName).toBe('stopping');
        return Promise.resolve();
      });

      Object.defineProperty(instance, 'currentStateName', {
        value: 'cnh',
        writable: true
      });

      const options = {
        oi: 10
      };
      await instance.stop(options, 'po');
      expect(instance.canStop).toHaveBeenCalled();
      expect(instance.currentStateName).toBe('initialized');
      expect(instance['_handleStop']).toHaveBeenCalledTimes(1);
      expect(instance['_handleStop']).toHaveBeenCalledWith(options, 'po', {
        logger: jasmine.any(Object)
      });

      expect(instance.emit).toHaveBeenCalledTimes(2);
      expect(instance.emit).toHaveBeenCalledWith('stopping', {_origin: instance});
      expect(instance.emit).toHaveBeenCalledWith('stopped', {_origin: instance});
      expect(instance['_addActivatedStateName']).not.toHaveBeenCalled();
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledTimes(2);
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledWith('running');
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledWith('paused');
    });
  }); // #stop

  describe('#_handleStop', function () {
    it("Then must return success", async function () {
      const instance = createInstance();

      await instance['_handleStop']({
        logger: new proxyLoggerLib.tests.mocks.SimpleGenericProxyLogger()
      });
      expect(true).toBeTruthy();
    });
  }); // #_handleStop

  describe('#canStop', function () {
    it("Given no activated Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(false);
      expect(instance.canStop()).toBe(false);
      ['paused', 'running'].forEach(item => {
        expect(instance.isStateActivated).toHaveBeenCalledWith(item);
      });
    });

    it("Given paused Then must return true", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValues(true, false);
      expect(instance.canStop()).toBe(true);
      ['paused', 'running'].forEach(item => {
        expect(instance.isStateActivated).toHaveBeenCalledWith(item);
      });
    });

    it("Given running Then must return true", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValues( false, true);
      expect(instance.canStop()).toBe(true);
      ['paused', 'running'].forEach(item => {
        expect(instance.isStateActivated).toHaveBeenCalledWith(item);
      });
    });
  }); // #canStop

  describe('#canPause', function () {
    it("Given isPauseAuthorized=false Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isPauseAuthorized').and.returnValue(false);
      spyOn(instance, 'isStateActivated').and.returnValue(false);

      expect(instance.canPause()).toBe(false);
      expect(instance.isPauseAuthorized).toHaveBeenCalled();
    });

    it("Given isPauseAuthorized=true and not running Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isPauseAuthorized').and.returnValue(true);
      spyOn(instance, 'isStateActivated').and.returnValue(false);

      expect(instance.canPause()).toBe(false);
      expect(instance.isPauseAuthorized).toHaveBeenCalled();
      expect(instance.isStateActivated).toHaveBeenCalledTimes(1);
      expect(instance.isStateActivated).toHaveBeenCalledWith('running');
    });

    it("Given isPauseAuthorized=true and running Then must return true", function () {
      const instance = createInstance();

      spyOn(instance, 'isPauseAuthorized').and.returnValue(true);
      spyOn(instance, 'isStateActivated').and.returnValue(true);

      expect(instance.canPause()).toBe(true);
      expect(instance.isPauseAuthorized).toHaveBeenCalled();
      expect(instance.isStateActivated).toHaveBeenCalledTimes(1);
      expect(instance.isStateActivated).toHaveBeenCalledWith('running');
    });
  }); // #canPause

  describe('#isPauseAuthorized', function () {
    it("Then must return false", function () {
      const instance = createInstance();

      expect(instance.isPauseAuthorized()).toBe(false);
    });
  }); // #isPauseAuthorized

  describe('#pause', function () {
    it("Given cannot pause Then must return error", async function () {
      const instance = createInstance();

      spyOn(instance, 'canPause').and.returnValue(false);
      // @ts-ignore
      spyOn(instance, '_handlePause').and.callFake(() => Promise.reject(new Error('Must not be called')));

      try {
        await instance.pause({});
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toEqual(jasmine.any(commonErrors.IllegalStateError));
        expect(instance.canPause).toHaveBeenCalled();
        expect(instance['_handlePause']).not.toHaveBeenCalled();
      }
    });

    it("Given _handlePause to fail Then must fail", async function () {
      const instance = createInstance();

      spyOn(instance, 'canPause').and.returnValue(true);
      spyOn(instance, 'emit');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');

      const expectedError = new Error('Unit Test - Fake Error');
      // @ts-ignore
      spyOn(instance, '_handlePause').and.callFake(() => {
        expect(instance.currentStateName).toBe('pausing');
        return Promise.reject(expectedError);
      });

      Object.defineProperty(instance, 'currentStateName', {
        value: 'cnh',
        writable: true
      });

      const options = {
        oi: 10
      };
      try {
        await instance.pause(options, 'po');
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toBe(expectedError);
        expect(instance.canPause).toHaveBeenCalled();
        expect(instance.currentStateName).toBe('cnh');
        expect(instance['_handlePause']).toHaveBeenCalledTimes(1);
        expect(instance['_handlePause']).toHaveBeenCalledWith(options, 'po', {
          logger: jasmine.any(Object)
        });

        expect(instance.emit).toHaveBeenCalledTimes(2);
        expect(instance.emit).toHaveBeenCalledWith('pausing', {_origin: instance});
        expect(instance.emit).toHaveBeenCalledWith('pauseFailed', {
          _origin: instance,
          error: expectedError
        });
        expect(instance['_addActivatedStateName']).not.toHaveBeenCalled();
        expect(instance['_removeActivatedStateName']).not.toHaveBeenCalled();
      }
    });

    it("Given _handlePause to success Then must success", async function () {
      const instance = createInstance();

      spyOn(instance, 'canPause').and.returnValue(true);
      spyOn(instance, 'emit');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');

      // @ts-ignore
      spyOn(instance, '_handlePause').and.callFake(() => {
        expect(instance.currentStateName).toBe('pausing');
        return Promise.resolve();
      });

      Object.defineProperty(instance, 'currentStateName', {
        value: 'cnh',
        writable: true
      });

      const options = {
        oi: 10
      };
      await instance.pause(options, 'po');
      expect(instance.canPause).toHaveBeenCalled();
      expect(instance.currentStateName).toBe('paused');
      expect(instance['_handlePause']).toHaveBeenCalledTimes(1);
      expect(instance['_handlePause']).toHaveBeenCalledWith(options, 'po', {
        logger: jasmine.any(Object)
      });

      expect(instance.emit).toHaveBeenCalledTimes(2);
      expect(instance.emit).toHaveBeenCalledWith('pausing', {_origin: instance});
      expect(instance.emit).toHaveBeenCalledWith('paused', {_origin: instance});
      expect(instance['_addActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(instance['_addActivatedStateName']).toHaveBeenCalledWith('paused');
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledWith('running');
    });
  }); // #pause

  describe('#_handlePause', function () {
    it("Then must return success", async function () {
      const instance = createInstance();

      await instance['_handlePause']({
        logger: new proxyLoggerLib.tests.mocks.SimpleGenericProxyLogger()
      });
      expect(true).toBeTruthy();
    });
  }); // #_handlePause

  describe('#canResume', function () {
    it("Given no paused Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(false);

      expect(instance.canResume()).toBe(false);
      expect(instance.isStateActivated).toHaveBeenCalledTimes(1);
      expect(instance.isStateActivated).toHaveBeenCalledWith('paused');
    });

    it("Given paused Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(true);

      expect(instance.canResume()).toBe(true);
      expect(instance.isStateActivated).toHaveBeenCalledTimes(1);
      expect(instance.isStateActivated).toHaveBeenCalledWith('paused');
    });
  }); // #canResume

  describe('#resume', function () {
    it("Given cannot resume Then must return error", async function () {
      const instance = createInstance();

      spyOn(instance, 'canResume').and.returnValue(false);
      // @ts-ignore
      spyOn(instance, '_handleResume').and.callFake(() => Promise.reject(new Error('Must not be called')));

      try {
        await instance.resume({});
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toEqual(jasmine.any(commonErrors.IllegalStateError));
        expect(instance.canResume).toHaveBeenCalled();
        expect(instance['_handleResume']).not.toHaveBeenCalled();
      }
    });

    it("Given _handlePause to fail Then must fail", async function () {
      const instance = createInstance();

      spyOn(instance, 'canResume').and.returnValue(true);
      spyOn(instance, 'emit');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');

      const expectedError = new Error('Unit Test - Fake Error');
      // @ts-ignore
      spyOn(instance, '_handleResume').and.callFake(() => {
        expect(instance.currentStateName).toBe('resuming');
        return Promise.reject(expectedError);
      });

      Object.defineProperty(instance, 'currentStateName', {
        value: 'cnh',
        writable: true
      });

      const options = {
        oi: 10
      };
      try {
        await instance.resume(options, 'po');
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toBe(expectedError);
        expect(instance.canResume).toHaveBeenCalled();
        expect(instance.currentStateName).toBe('cnh');
        expect(instance['_handleResume']).toHaveBeenCalledTimes(1);
        expect(instance['_handleResume']).toHaveBeenCalledWith(options, 'po', {
          logger: jasmine.any(Object)
        });

        expect(instance.emit).toHaveBeenCalledTimes(2);
        expect(instance.emit).toHaveBeenCalledWith('resuming', {_origin: instance});
        expect(instance.emit).toHaveBeenCalledWith('resumeFailed', {
          _origin: instance,
          error: expectedError
        });
        expect(instance['_addActivatedStateName']).not.toHaveBeenCalled();
        expect(instance['_removeActivatedStateName']).not.toHaveBeenCalled();
      }
    });

    it("Given _handlePause to success Then must success", async function () {
      const instance = createInstance();

      spyOn(instance, 'canResume').and.returnValue(true);
      spyOn(instance, 'emit');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');

      // @ts-ignore
      spyOn(instance, '_handleResume').and.callFake(() => {
        expect(instance.currentStateName).toBe('resuming');
        return Promise.resolve();
      });

      Object.defineProperty(instance, 'currentStateName', {
        value: 'cnh',
        writable: true
      });

      const options = {
        oi: 10
      };
      await instance.resume(options, 'po');
      expect(instance.canResume).toHaveBeenCalled();
      expect(instance.currentStateName).toBe('running');
      expect(instance['_handleResume']).toHaveBeenCalledTimes(1);
      expect(instance['_handleResume']).toHaveBeenCalledWith(options, 'po', {
        logger: jasmine.any(Object)
      });

      expect(instance.emit).toHaveBeenCalledTimes(2);
      expect(instance.emit).toHaveBeenCalledWith('resuming', {_origin: instance});
      expect(instance.emit).toHaveBeenCalledWith('resumed', {_origin: instance});
      expect(instance['_addActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(instance['_addActivatedStateName']).toHaveBeenCalledWith('running');
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledWith('paused');
    });
  }); // #resume

  describe('#_handleResume', function () {
    it("Then must return success", async function () {
      const instance = createInstance();

      await instance['_handleResume']({
        logger: new proxyLoggerLib.tests.mocks.SimpleGenericProxyLogger()
      });
      expect(true).toBeTruthy();
    });
  }); // #_handleResume

  describe('#_handleRunDone', function () {
    it("Then must change state and emit signals", async function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      spyOn(instance, 'emit');

      Object.defineProperty(instance, 'currentStateName', {
        value: 'ok',
        writable: true
      });

      const result = {
        p: 105
      };
      await instance['_handleRunDone'](result, 'po');
      expect(instance['_addActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(instance['_addActivatedStateName']).toHaveBeenCalledWith('finished');
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledWith('running');
      expect(instance.currentStateName).toBe('finished');
      expect(instance.emit).toHaveBeenCalledTimes(1);
      expect(instance.emit).toHaveBeenCalledWith('finished', {
        _origin: instance,
        result
      });
    });
  }); // #_handleRunDone

  describe('#_handleExecutionFailed', function () {
    it("Then must change state and emit signals", async function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      spyOn(instance, 'emit');

      Object.defineProperty(instance, 'currentStateName', {
        value: 'ok',
        writable: true
      });

      const error = new Error('Unit Test - Fake Error');
      await instance['_handleExecutionFailed'](error, 'po');
      expect(instance['_addActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(instance['_addActivatedStateName']).toHaveBeenCalledWith('finished');
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledWith('running');
      expect(instance.currentStateName).toBe('finished');
      expect(instance.emit).toHaveBeenCalledTimes(1);
      expect(instance.emit).toHaveBeenCalledWith('finishedWithError', {
        _origin: instance,
        error
      });
    });
  }); // #_handleExecutionFailed

  describe('#_handleFinalization', function () {

    it("Given not started Then must only call super", async function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(BaseInitializableObject.prototype, '_handleFinalization').and.callFake(() => Promise.resolve({lo: 105}));
      spyOn(instance, 'canStop').and.returnValue(false);
      spyOn(instance, 'stop').and.callFake(() => Promise.reject(new Error('Must not be called')));

      const args = [
        'a',
        'p',
        {
          logger: new proxyLoggerLib.tests.mocks.SimpleGenericProxyLogger()
        }
      ];
      await instance['_handleFinalization'](...args);
      expect(BaseInitializableObject.prototype['_handleFinalization']).toHaveBeenCalledTimes(1);
      expect(BaseInitializableObject.prototype['_handleFinalization']).toHaveBeenCalledWith(...args);
      expect(instance.canStop).toHaveBeenCalled();
      expect(instance.stop).not.toHaveBeenCalled();
    });

    it("Given started Then must call unstart and super", async function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(BaseInitializableObject.prototype, '_handleFinalization').and.callFake(() => Promise.resolve({lo: 105}));
      spyOn(instance, 'canStop').and.returnValue(true);
      spyOn(instance, 'stop').and.callFake(() => Promise.resolve());

      const args = [
        'a',
        'p',
        {
          logger: new proxyLoggerLib.tests.mocks.SimpleGenericProxyLogger()
        }
      ];
      await instance['_handleFinalization'](...args);
      expect(BaseInitializableObject.prototype['_handleFinalization']).toHaveBeenCalledTimes(1);
      expect(BaseInitializableObject.prototype['_handleFinalization']).toHaveBeenCalledWith(...args);
      expect(instance.canStop).toHaveBeenCalled();
      expect(instance.stop).toHaveBeenCalledTimes(1);
      expect(instance.stop).toHaveBeenCalledWith(...args);
    });
  }); // #_handleFinalization

  describe('#_acceptStateName', function () {
    it("Given not valid value Then must return false", function () {
      const instance = createInstance();

      spyOn(BaseRunnableObject, 'getStateNames').and.returnValue([]);
      expect(instance['_acceptStateName']('sls')).toBe(false);
      expect(BaseRunnableObject.getStateNames).toHaveBeenCalled();
    });

    it("Given valid value Then must return true", function () {
      const instance = createInstance();

      spyOn(BaseRunnableObject, 'getStateNames').and.returnValue(['dd', 'sls', 'po']);
      expect(instance['_acceptStateName']('sls')).toBe(true);
      expect(BaseRunnableObject.getStateNames).toHaveBeenCalled();
    });
  }); // #_acceptStateName

  describe('#getStateNames', function () {
    const _ = require('lodash');

    it("Then must return expected value", function () {
      expect(BaseRunnableObject.getStateNames()).toEqual(jasmine.arrayContaining(_.concat([
        'starting',
        'running',
        'pausing',
        'paused',
        'resuming',
        'stopping',
        'finished'
      ], BaseInitializableObject.getStateNames())));
    });
  }); // #getStateNames

  describe('#getSignalNames', function () {
    const _ = require('lodash');

    it("Then must return expected value", function () {
      expect(BaseRunnableObject.getSignalNames()).toEqual(jasmine.arrayContaining(_.concat([
        'starting',
        'startFailed',
        'started',
        'paused',
        'pausing',
        'pauseFailed',
        'resuming',
        'resumed',
        'resumeFailed',
        'stopping',
        'stopFailed',
        'stopped',
        'finished',
        'finishedWithError'
      ], BaseInitializableObject.getSignalNames())));
    });
  }); // #getSignalNames
}); // #BaseRunnableObject
