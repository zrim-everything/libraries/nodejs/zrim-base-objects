import {BaseLoadableObject} from '../../../lib/base-loadable-object';
import {common as commonErrors} from 'zrim-errors';
import * as proxyLoggerLib from 'zrim-proxy-logger';
import {BaseInitializableObject} from "../../../lib/base-initializable-object";

describe('#BaseLoadableObject', function () {
  function createInstance(): BaseLoadableObject {
    return new BaseLoadableObject();
  }

  describe('#load', function () {
    it("Given cannot load Then must return error", async function () {
      const instance = createInstance();

      spyOn(instance, 'canLoad').and.returnValue(false);
      // @ts-ignore
      spyOn(instance, '_handleLoad').and.callFake(() => Promise.reject(new Error('Must not be called')));

      try {
        await instance.load({});
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toEqual(jasmine.any(commonErrors.IllegalStateError));
        expect(instance.canLoad).toHaveBeenCalled();
        expect(instance['_handleLoad']).not.toHaveBeenCalled();
      }
    });

    it("Given _handleLoad to fail Then must fail", async function () {
      const instance = createInstance();

      spyOn(instance, 'canLoad').and.returnValue(true);
      spyOn(instance, 'emit');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');

      const expectedError = new Error('Unit Test - Fake Error');
      // @ts-ignore
      spyOn(instance, '_handleLoad').and.callFake(() => {
        expect(instance.currentStateName).toBe('loading');
        return Promise.reject(expectedError);
      });

      Object.defineProperty(instance, 'currentStateName', {
        value: 'cnh',
        writable: true
      });

      const options = {
        oi: 10
      };
      try {
        await instance.load(options, 'po');
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toBe(expectedError);
        expect(instance.canLoad).toHaveBeenCalled();
        expect(instance.currentStateName).toBe('cnh');
        expect(instance['_handleLoad']).toHaveBeenCalledTimes(1);
        expect(instance['_handleLoad']).toHaveBeenCalledWith(options, 'po', {
          logger: jasmine.any(Object)
        });

        expect(instance.emit).toHaveBeenCalledTimes(2);
        expect(instance.emit).toHaveBeenCalledWith('loading', {_origin: instance});
        expect(instance.emit).toHaveBeenCalledWith('loadFailed', {
          _origin: instance,
          error: expectedError
        });
        expect(instance['_addActivatedStateName']).not.toHaveBeenCalled();
        expect(instance['_removeActivatedStateName']).not.toHaveBeenCalled();
      }
    });

    it("Given _handleLoad to success Then must success", async function () {
      const instance = createInstance();

      spyOn(instance, 'canLoad').and.returnValue(true);
      spyOn(instance, 'emit');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');

      // @ts-ignore
      spyOn(instance, '_handleLoad').and.callFake(() => {
        expect(instance.currentStateName).toBe('loading');
        return Promise.resolve();
      });

      Object.defineProperty(instance, 'currentStateName', {
        value: 'cnh',
        writable: true
      });

      const options = {
        oi: 10
      };
      await instance.load(options, 'po');
      expect(instance.canLoad).toHaveBeenCalled();
      expect(instance.currentStateName).toBe('loaded');
      expect(instance['_handleLoad']).toHaveBeenCalledTimes(1);
      expect(instance['_handleLoad']).toHaveBeenCalledWith(options, 'po', {
        logger: jasmine.any(Object)
      });

      expect(instance.emit).toHaveBeenCalledTimes(2);
      expect(instance.emit).toHaveBeenCalledWith('loading', {_origin: instance});
      expect(instance.emit).toHaveBeenCalledWith('loaded', {_origin: instance});
      expect(instance['_addActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(instance['_addActivatedStateName']).toHaveBeenCalledWith('loaded');
      expect(instance['_removeActivatedStateName']).not.toHaveBeenCalled();
    });
  }); // #load

  describe('#_handleLoad', function () {
    it("Then must return success", async function () {
      const instance = createInstance();

      await instance['_handleLoad']({
        logger: new proxyLoggerLib.tests.mocks.SimpleGenericProxyLogger()
      });
      expect(true).toBeTruthy();
    });
  }); // #_handleLoad

  describe('#canLoad', function () {
    it("Given not initialized and not loaded Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(false);
      expect(instance.canLoad()).toBe(false);
      expect(instance.isStateActivated).toHaveBeenCalledTimes(1);
      expect(instance.isStateActivated).toHaveBeenCalledWith('initialized');
    });

    it("Given activated initialized and loaded Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValues(true, true);
      expect(instance.canLoad()).toBe(false);
      expect(instance.isStateActivated).toHaveBeenCalledTimes(2);
      expect(instance.isStateActivated).toHaveBeenCalledWith('initialized');
      expect(instance.isStateActivated).toHaveBeenCalledWith('loaded');
    });

    it("Given activated loaded and not initialized Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValues(false, true);
      expect(instance.canLoad()).toBe(false);
      expect(instance.isStateActivated).toHaveBeenCalledTimes(1);
      expect(instance.isStateActivated).toHaveBeenCalledWith('initialized');
    });

    it("Given activated initialized and not loaded Then must return true", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValues(true, false);
      expect(instance.canLoad()).toBe(true);
      expect(instance.isStateActivated).toHaveBeenCalledWith('initialized');
      expect(instance.isStateActivated).toHaveBeenCalledWith('loaded');
    });
  }); // #canInitialize

  describe('#isLoaded', function () {
    it("Given no activated Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(false);
      expect(instance.isLoaded()).toBe(false);
      expect(instance.isStateActivated).toHaveBeenCalledWith('loaded');
    });

    it("Given activated Then must return true", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(true);
      expect(instance.isLoaded()).toBe(true);
      expect(instance.isStateActivated).toHaveBeenCalledWith('loaded');
    });
  }); // #isLoaded

  describe('#unLoad', function () {
    it("Given cannot unLoad Then must return error", async function () {
      const instance = createInstance();

      spyOn(instance, 'canUnLoad').and.returnValue(false);
      // @ts-ignore
      spyOn(instance, '_handleUnLoad').and.callFake(() => Promise.reject(new Error('Must not be called')));

      try {
        await instance.unLoad({});
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toEqual(jasmine.any(commonErrors.IllegalStateError));
        expect(instance.canUnLoad).toHaveBeenCalled();
        expect(instance['_handleUnLoad']).not.toHaveBeenCalled();
      }
    });

    it("Given _handleUnLoad to fail Then must fail", async function () {
      const instance = createInstance();

      spyOn(instance, 'canUnLoad').and.returnValue(true);
      spyOn(instance, 'emit');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');

      const expectedError = new Error('Unit Test - Fake Error');
      // @ts-ignore
      spyOn(instance, '_handleUnLoad').and.callFake(() => {
        expect(instance.currentStateName).toBe('unLoading');
        return Promise.reject(expectedError);
      });

      Object.defineProperty(instance, 'currentStateName', {
        value: 'cnh',
        writable: true
      });

      const options = {
        oi: 10
      };
      try {
        await instance.unLoad(options, 'po');
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toBe(expectedError);
        expect(instance.canUnLoad).toHaveBeenCalled();
        expect(instance.currentStateName).toBe('cnh');
        expect(instance['_handleUnLoad']).toHaveBeenCalledTimes(1);
        expect(instance['_handleUnLoad']).toHaveBeenCalledWith(options, 'po', {
          logger: jasmine.any(Object)
        });

        expect(instance.emit).toHaveBeenCalledTimes(2);
        expect(instance.emit).toHaveBeenCalledWith('unLoading', {_origin: instance});
        expect(instance.emit).toHaveBeenCalledWith('unLoadFailed', {
          _origin: instance,
          error: expectedError
        });
        expect(instance['_addActivatedStateName']).toHaveBeenCalledTimes(1);
        expect(instance['_addActivatedStateName']).toHaveBeenCalledWith('loaded');
        expect(instance['_removeActivatedStateName']).toHaveBeenCalledTimes(1);
        expect(instance['_removeActivatedStateName']).toHaveBeenCalledWith('loaded');
      }
    });

    it("Given _handleUnLoad to success Then must success", async function () {
      const instance = createInstance();

      spyOn(instance, 'canUnLoad').and.returnValue(true);
      spyOn(instance, 'emit');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');

      // @ts-ignore
      spyOn(instance, '_handleUnLoad').and.callFake(() => {
        expect(instance.currentStateName).toBe('unLoading');
        return Promise.resolve();
      });

      Object.defineProperty(instance, 'currentStateName', {
        value: 'cnh',
        writable: true
      });

      const options = {
        oi: 10
      };
      await instance.unLoad(options, 'po');
      expect(instance.canUnLoad).toHaveBeenCalled();
      expect(instance.currentStateName).toBe('initialized');
      expect(instance['_handleUnLoad']).toHaveBeenCalledTimes(1);
      expect(instance['_handleUnLoad']).toHaveBeenCalledWith(options, 'po', {
        logger: jasmine.any(Object)
      });

      expect(instance.emit).toHaveBeenCalledTimes(2);
      expect(instance.emit).toHaveBeenCalledWith('unLoading', {_origin: instance});
      expect(instance.emit).toHaveBeenCalledWith('unLoaded', {_origin: instance});
      expect(instance['_addActivatedStateName']).not.toHaveBeenCalled();
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledWith('loaded');
    });
  }); // #unLoad

  describe('#_handleUnLoad', function () {
    it("Then must return success", async function () {
      const instance = createInstance();

      await instance['_handleUnLoad']({
        logger: new proxyLoggerLib.tests.mocks.SimpleGenericProxyLogger()
      });
      expect(true).toBeTruthy();
    });
  }); // #_handleUnLoad

  describe('#canUnLoad', function () {
    it("Given no activated Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(false);
      expect(instance.canUnLoad()).toBe(false);
      expect(instance.isStateActivated).toHaveBeenCalledWith('loaded');
    });

    it("Given activated Then must return true", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(true);
      expect(instance.canUnLoad()).toBe(true);
      expect(instance.isStateActivated).toHaveBeenCalledWith('loaded');
    });
  }); // #canUnLoad

  describe('#_handleFinalization', function () {
    it("Given not loaded Then must only call super", async function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(BaseInitializableObject.prototype, '_handleFinalization').and.callFake(() => Promise.resolve({lo: 105}));
      spyOn(instance, 'isLoaded').and.returnValue(false);
      spyOn(instance, 'unLoad').and.callFake(() => Promise.reject(new Error('Must not be called')));

      const args = [
        'a',
        'p',
        {
          logger: new proxyLoggerLib.tests.mocks.SimpleGenericProxyLogger()
        }
      ];
      await instance['_handleFinalization'](...args);
      expect(BaseInitializableObject.prototype['_handleFinalization']).toHaveBeenCalledTimes(1);
      expect(BaseInitializableObject.prototype['_handleFinalization']).toHaveBeenCalledWith(...args);
      expect(instance.isLoaded).toHaveBeenCalled();
      expect(instance.unLoad).not.toHaveBeenCalled();
    });

    it("Given loaded Then must call unload and super", async function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(BaseInitializableObject.prototype, '_handleFinalization').and.callFake(() => Promise.resolve({lo: 105}));
      spyOn(instance, 'isLoaded').and.returnValue(true);
      spyOn(instance, 'unLoad').and.callFake(() => Promise.resolve());

      const args = [
        'a',
        'p',
        {
          logger: new proxyLoggerLib.tests.mocks.SimpleGenericProxyLogger()
        }
      ];
      await instance['_handleFinalization'](...args);
      expect(BaseInitializableObject.prototype['_handleFinalization']).toHaveBeenCalledTimes(1);
      expect(BaseInitializableObject.prototype['_handleFinalization']).toHaveBeenCalledWith(...args);
      expect(instance.isLoaded).toHaveBeenCalled();
      expect(instance.unLoad).toHaveBeenCalledTimes(1);
      expect(instance.unLoad).toHaveBeenCalledWith(...args);
    });
  }); // #_handleFinalization

  describe('#_acceptStateName', function () {
    it("Given not valid value Then must return false", function () {
      const instance = createInstance();

      spyOn(BaseLoadableObject, 'getStateNames').and.returnValue([]);
      expect(instance['_acceptStateName']('sls')).toBe(false);
      expect(BaseLoadableObject.getStateNames).toHaveBeenCalled();
    });

    it("Given valid value Then must return true", function () {
      const instance = createInstance();

      spyOn(BaseLoadableObject, 'getStateNames').and.returnValue(['dd', 'sls', 'po']);
      expect(instance['_acceptStateName']('sls')).toBe(true);
      expect(BaseLoadableObject.getStateNames).toHaveBeenCalled();
    });
  }); // #_acceptStateName

  describe('#getStateNames', function () {
    const _ = require('lodash');

    it("Then must return expected value", function () {
      expect(BaseLoadableObject.getStateNames()).toEqual(jasmine.arrayContaining(_.concat([
        'loading',
        'unLoading',
        'loaded'
      ], BaseInitializableObject.getStateNames())));
    });
  }); // #getStateNames

  describe('#getSignalNames', function () {
    const _ = require('lodash');

    it("Then must return expected value", function () {
      expect(BaseLoadableObject.getSignalNames()).toEqual(jasmine.arrayContaining(_.concat([
        'loading',
        'loadFailed',
        'loaded',
        'unLoading',
        'unLoadFailed',
        'unLoaded'
      ], BaseInitializableObject.getSignalNames())));
    });
  }); // #getSignalNames
}); // #BaseLoadableObject
