import {BaseConnectableObject} from '../../../lib/base-connectable-object';
import {common as commonErrors} from 'zrim-errors';
import * as proxyLoggerLib from 'zrim-proxy-logger';
import {BaseInitializableObject} from "../../../lib/base-initializable-object";


describe('#BaseConnectableObject', function () {
  function createInstance(): BaseConnectableObject {
    return new BaseConnectableObject();
  }

  describe('#connect', function () {
    it("Given cannot connect Then must return error", async function () {
      const instance = createInstance();

      spyOn(instance, 'canConnect').and.returnValue(false);
      // @ts-ignore
      spyOn(instance, '_handleConnection').and.callFake(() => Promise.reject(new Error('Must not be called')));

      try {
        await instance.connect({});
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toEqual(jasmine.any(commonErrors.IllegalStateError));
        expect(instance.canConnect).toHaveBeenCalled();
        expect(instance['_handleConnection']).not.toHaveBeenCalled();
      }
    });

    it("Given _handleConnection to fail Then must fail", async function () {
      const instance = createInstance();

      spyOn(instance, 'canConnect').and.returnValue(true);
      spyOn(instance, 'emit');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');

      const expectedError = new Error('Unit Test - Fake Error');
      // @ts-ignore
      spyOn(instance, '_handleConnection').and.callFake(() => {
        expect(instance.currentStateName).toBe('connecting');
        return Promise.reject(expectedError);
      });

      Object.defineProperty(instance, 'currentStateName', {
        value: 'cnh',
        writable: true
      });

      const options = {
        oi: 10
      };
      try {
        await instance.connect(options, 'po');
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toBe(expectedError);
        expect(instance.canConnect).toHaveBeenCalled();
        expect(instance.currentStateName).toBe('cnh');
        expect(instance['_handleConnection']).toHaveBeenCalledTimes(1);
        expect(instance['_handleConnection']).toHaveBeenCalledWith(options, 'po', {
          logger: jasmine.any(Object)
        });

        expect(instance.emit).toHaveBeenCalledTimes(2);
        expect(instance.emit).toHaveBeenCalledWith('connecting', {_origin: instance});
        expect(instance.emit).toHaveBeenCalledWith('connectionFailed', {
          _origin: instance,
          error: expectedError
        });
        expect(instance['_addActivatedStateName']).not.toHaveBeenCalled();
        expect(instance['_removeActivatedStateName']).not.toHaveBeenCalled();
      }
    });

    it("Given _handleConnection to success Then must success", async function () {
      const instance = createInstance();

      spyOn(instance, 'canConnect').and.returnValue(true);
      spyOn(instance, 'emit');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');

      // @ts-ignore
      spyOn(instance, '_handleConnection').and.callFake(() => {
        expect(instance.currentStateName).toBe('connecting');
        return Promise.resolve();
      });

      Object.defineProperty(instance, 'currentStateName', {
        value: 'cnh',
        writable: true
      });

      const options = {
        oi: 10
      };
      await instance.connect(options, 'po');
      expect(instance.canConnect).toHaveBeenCalled();
      expect(instance.currentStateName).toBe('connected');
      expect(instance['_handleConnection']).toHaveBeenCalledTimes(1);
      expect(instance['_handleConnection']).toHaveBeenCalledWith(options, 'po', {
        logger: jasmine.any(Object)
      });

      expect(instance.emit).toHaveBeenCalledTimes(2);
      expect(instance.emit).toHaveBeenCalledWith('connecting', {_origin: instance});
      expect(instance.emit).toHaveBeenCalledWith('connected', {_origin: instance});
      expect(instance['_addActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(instance['_addActivatedStateName']).toHaveBeenCalledWith('connected');
      expect(instance['_removeActivatedStateName']).not.toHaveBeenCalled();
    });
  }); // #connect

  describe('#_handleConnection', function () {
    it("Then must return success", async function () {
      const instance = createInstance();

      await instance['_handleConnection']({
        logger: new proxyLoggerLib.tests.mocks.SimpleGenericProxyLogger()
      });
      expect(true).toBeTruthy();
    });
  }); // #_handleConnection

  describe('#canConnect', function () {
    it("Given not initialized and not connected Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(false);
      expect(instance.canConnect()).toBe(false);
      expect(instance.isStateActivated).toHaveBeenCalledTimes(1);
      expect(instance.isStateActivated).toHaveBeenCalledWith('initialized');
    });

    it("Given activated initialized and connected Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValues(true, true);
      expect(instance.canConnect()).toBe(false);
      expect(instance.isStateActivated).toHaveBeenCalledTimes(2);
      expect(instance.isStateActivated).toHaveBeenCalledWith('initialized');
      expect(instance.isStateActivated).toHaveBeenCalledWith('connected');
    });

    it("Given activated connected and not initialized Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValues(false, true);
      expect(instance.canConnect()).toBe(false);
      expect(instance.isStateActivated).toHaveBeenCalledTimes(1);
      expect(instance.isStateActivated).toHaveBeenCalledWith('initialized');
    });

    it("Given activated initialized and not connected Then must return true", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValues(true, false);
      expect(instance.canConnect()).toBe(true);
      expect(instance.isStateActivated).toHaveBeenCalledWith('initialized');
      expect(instance.isStateActivated).toHaveBeenCalledWith('connected');
    });
  }); // #canInitialize

  describe('#isConnected', function () {
    it("Given no activated Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(false);
      expect(instance.isConnected()).toBe(false);
      expect(instance.isStateActivated).toHaveBeenCalledWith('connected');
    });

    it("Given activated Then must return true", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(true);
      expect(instance.isConnected()).toBe(true);
      expect(instance.isStateActivated).toHaveBeenCalledWith('connected');
    });
  }); // #isConnected

  describe('#disconnect', function () {
    it("Given cannot disconnect Then must return error", async function () {
      const instance = createInstance();

      spyOn(instance, 'canDisconnect').and.returnValue(false);
      // @ts-ignore
      spyOn(instance, '_handleDisconnection').and.callFake(() => Promise.reject(new Error('Must not be called')));

      try {
        await instance.disconnect({});
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toEqual(jasmine.any(commonErrors.IllegalStateError));
        expect(instance.canDisconnect).toHaveBeenCalled();
        expect(instance['_handleDisconnection']).not.toHaveBeenCalled();
      }
    });

    it("Given _handleDisconnection to fail Then must fail", async function () {
      const instance = createInstance();

      spyOn(instance, 'canDisconnect').and.returnValue(true);
      spyOn(instance, 'emit');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');

      const expectedError = new Error('Unit Test - Fake Error');
      // @ts-ignore
      spyOn(instance, '_handleDisconnection').and.callFake(() => {
        expect(instance.currentStateName).toBe('disconnecting');
        return Promise.reject(expectedError);
      });

      Object.defineProperty(instance, 'currentStateName', {
        value: 'cnh',
        writable: true
      });

      const options = {
        oi: 10
      };
      try {
        await instance.disconnect(options, 'po');
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toBe(expectedError);
        expect(instance.canDisconnect).toHaveBeenCalled();
        expect(instance.currentStateName).toBe('cnh');
        expect(instance['_handleDisconnection']).toHaveBeenCalledTimes(1);
        expect(instance['_handleDisconnection']).toHaveBeenCalledWith(options, 'po', {
          logger: jasmine.any(Object)
        });

        expect(instance.emit).toHaveBeenCalledTimes(2);
        expect(instance.emit).toHaveBeenCalledWith('disconnecting', {_origin: instance});
        expect(instance.emit).toHaveBeenCalledWith('disconnectionFailed', {
          _origin: instance,
          error: expectedError
        });
        expect(instance['_addActivatedStateName']).toHaveBeenCalledTimes(1);
        expect(instance['_addActivatedStateName']).toHaveBeenCalledWith('connected');
        expect(instance['_removeActivatedStateName']).toHaveBeenCalledTimes(1);
        expect(instance['_removeActivatedStateName']).toHaveBeenCalledWith('connected');
      }
    });

    it("Given _handleDisconnection to success Then must success", async function () {
      const instance = createInstance();

      spyOn(instance, 'canDisconnect').and.returnValue(true);
      spyOn(instance, 'emit');
      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');

      // @ts-ignore
      spyOn(instance, '_handleDisconnection').and.callFake(() => {
        expect(instance.currentStateName).toBe('disconnecting');
        return Promise.resolve();
      });

      Object.defineProperty(instance, 'currentStateName', {
        value: 'cnh',
        writable: true
      });

      const options = {
        oi: 10
      };
      await instance.disconnect(options, 'po');
      expect(instance.canDisconnect).toHaveBeenCalled();
      expect(instance.currentStateName).toBe('initialized');
      expect(instance['_handleDisconnection']).toHaveBeenCalledTimes(1);
      expect(instance['_handleDisconnection']).toHaveBeenCalledWith(options, 'po', {
        logger: jasmine.any(Object)
      });

      expect(instance.emit).toHaveBeenCalledTimes(2);
      expect(instance.emit).toHaveBeenCalledWith('disconnecting', {_origin: instance});
      expect(instance.emit).toHaveBeenCalledWith('disconnected', {_origin: instance});
      expect(instance['_addActivatedStateName']).not.toHaveBeenCalled();
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledWith('connected');
    });
  }); // #disconnect

  describe('#_handleDisconnection', function () {
    it("Then must return success", async function () {
      const instance = createInstance();

      await instance['_handleDisconnection']({
        logger: new proxyLoggerLib.tests.mocks.SimpleGenericProxyLogger()
      });
      expect(true).toBeTruthy();
    });
  }); // #_handleDisconnection

  describe('#canDisconnect', function () {
    it("Given no activated Then must return false", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(false);
      expect(instance.canDisconnect()).toBe(false);
      expect(instance.isStateActivated).toHaveBeenCalledWith('connected');
    });

    it("Given activated Then must return true", function () {
      const instance = createInstance();

      spyOn(instance, 'isStateActivated').and.returnValue(true);
      expect(instance.canDisconnect()).toBe(true);
      expect(instance.isStateActivated).toHaveBeenCalledWith('connected');
    });
  }); // #canDisconnect

  describe('#_handleFinalization', function () {

    it("Given not connected Then must only call super", async function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(BaseInitializableObject.prototype, '_handleFinalization').and.callFake(() => Promise.resolve({lo: 105}));
      spyOn(instance, 'isConnected').and.returnValue(false);
      spyOn(instance, 'disconnect').and.callFake(() => Promise.reject(new Error('Must not be called')));

      const args = [
        'a',
        'p',
        {
          logger: new proxyLoggerLib.tests.mocks.SimpleGenericProxyLogger()
        }
      ];
      await instance['_handleFinalization'](...args);
      expect(BaseInitializableObject.prototype['_handleFinalization']).toHaveBeenCalledTimes(1);
      expect(BaseInitializableObject.prototype['_handleFinalization']).toHaveBeenCalledWith(...args);
      expect(instance.isConnected).toHaveBeenCalled();
      expect(instance.disconnect).not.toHaveBeenCalled();
    });

    it("Given connected Then must call unconnect and super", async function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(BaseInitializableObject.prototype, '_handleFinalization').and.callFake(() => Promise.resolve({lo: 105}));
      spyOn(instance, 'isConnected').and.returnValue(true);
      spyOn(instance, 'disconnect').and.callFake(() => Promise.resolve());

      const args = [
        'a',
        'p',
        {
          logger: new proxyLoggerLib.tests.mocks.SimpleGenericProxyLogger()
        }
      ];
      await instance['_handleFinalization'](...args);
      expect(BaseInitializableObject.prototype['_handleFinalization']).toHaveBeenCalledTimes(1);
      expect(BaseInitializableObject.prototype['_handleFinalization']).toHaveBeenCalledWith(...args);
      expect(instance.isConnected).toHaveBeenCalled();
      expect(instance.disconnect).toHaveBeenCalledTimes(1);
      expect(instance.disconnect).toHaveBeenCalledWith(...args);
    });
  }); // #_handleFinalization

  describe('#_onConnectionLost', function () {
    it("Then must remove the state connected and change state", async function () {
      const instance = createInstance();

      Object.defineProperty(instance, 'currentStateName', {
        value: 'ch',
        writable: true
      });

      // @ts-ignore
      spyOn(instance, '_removeActivatedStateName');
      spyOn(instance, 'emit');

      await instance['_onConnectionLost']();
      expect(instance.currentStateName).toBe('initialized');
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(instance['_removeActivatedStateName']).toHaveBeenCalledWith('connected');
      expect(instance.emit).toHaveBeenCalledTimes(1);
      expect(instance.emit).toHaveBeenCalledWith('connectionLost', {
        _origin: instance
      });
    });
  }); // #_onConnectionLost

  describe('#_onReconnected', function () {
    it("Then must remove the state connected and change state", async function () {
      const instance = createInstance();

      Object.defineProperty(instance, 'currentStateName', {
        value: 'ch',
        writable: true
      });

      // @ts-ignore
      spyOn(instance, '_addActivatedStateName');
      spyOn(instance, 'emit');

      await instance['_onReconnected']();
      expect(instance.currentStateName).toBe('connected');
      expect(instance['_addActivatedStateName']).toHaveBeenCalledTimes(1);
      expect(instance['_addActivatedStateName']).toHaveBeenCalledWith('connected');
      expect(instance.emit).toHaveBeenCalledTimes(1);
      expect(instance.emit).toHaveBeenCalledWith('reconnected', {
        _origin: instance
      });
    });
  }); // #_onReconnected

  describe('#_acceptStateName', function () {
    it("Given not valid value Then must return false", function () {
      const instance = createInstance();

      spyOn(BaseConnectableObject, 'getStateNames').and.returnValue([]);
      expect(instance['_acceptStateName']('sls')).toBe(false);
      expect(BaseConnectableObject.getStateNames).toHaveBeenCalled();
    });

    it("Given valid value Then must return true", function () {
      const instance = createInstance();

      spyOn(BaseConnectableObject, 'getStateNames').and.returnValue(['dd', 'sls', 'po']);
      expect(instance['_acceptStateName']('sls')).toBe(true);
      expect(BaseConnectableObject.getStateNames).toHaveBeenCalled();
    });
  }); // #_acceptStateName

  describe('#getStateNames', function () {
    const _ = require('lodash');

    it("Then must return expected value", function () {
      expect(BaseConnectableObject.getStateNames()).toEqual(jasmine.arrayContaining(_.concat([
        'connecting',
        'disconnecting',
        'connected'
      ], BaseInitializableObject.getStateNames())));
    });
  }); // #getStateNames

  describe('#getSignalNames', function () {
    const _ = require('lodash');

    it("Then must return expected value", function () {
      expect(BaseConnectableObject.getSignalNames()).toEqual(jasmine.arrayContaining(_.concat([
        'connecting',
        'connectionFailed',
        'connected',
        'disconnecting',
        'disconnectionFailed',
        'disconnected',
        'connectionLost',
        'reconnected'
      ], BaseInitializableObject.getSignalNames())));
    });
  }); // #getSignalNames
}); // #BaseConnectableObject
