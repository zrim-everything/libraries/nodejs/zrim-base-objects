import {
  InitializableObject
} from "./initializable-object";
import {zrimObject} from "./zrim-object";


/**
 * The load started
 *
 * @event LoadableObject#loading
 * @type {object}
 * @property {LoadableObject} _origin The origin instance
 */
/**
 * The load succeed
 *
 * @event LoadableObject#loaded
 * @type {object}
 * @property {LoadableObject} _origin The origin instance
 */
/**
 * The load failed
 *
 * @event LoadableObject#loadFailed
 * @type {object}
 * @property {LoadableObject} _origin The origin instance
 * @property {Error} error The error
 */
/**
 * The unload started
 *
 * @event LoadableObject#unLoading
 * @type {object}
 * @property {LoadableObject} _origin The origin instance
 */
/**
 * The unload succeed
 *
 * @event LoadableObject#unLoaded
 * @type {object}
 * @property {LoadableObject} _origin The origin instance
 */
/**
 * The unload failed
 *
 * @event LoadableObject#unLoadFailed
 * @type {object}
 * @property {LoadableObject} _origin The origin instance
 * @property {Error} error The error
 */

export namespace loadableObject {

  export interface LoadOptions {

  }

  export interface UnLoadOptions {

  }

  export namespace events {

    /**
     * Signal: loading
     */
    export interface Loading extends zrimObject.events.BaseEvent<LoadableObject> {

    }

    /**
     * Signal: loaded
     */
    export interface Loaded extends zrimObject.events.BaseEvent<LoadableObject> {

    }

    /**
     * Signal: loadFailed
     */
    export interface LoadFailed extends zrimObject.events.BaseEvent<LoadableObject> {

      /**
       * The error
       */
      error: Error;
    }

    /**
     * Signal: unLoading
     */
    export interface UnLoading extends zrimObject.events.BaseEvent<LoadableObject> {

    }

    /**
     * Signal: unLoaded
     */
    export interface UnLoaded extends zrimObject.events.BaseEvent<LoadableObject> {

    }

    /**
     * Signal: unLoadFailed
     */
    export interface UnLoadFailed extends zrimObject.events.BaseEvent<LoadableObject> {

      /**
       * The error
       */
      error: Error;
    }
  }
}

export interface LoadableObject extends InitializableObject {

  /**
   * Ask to load the instance with an options
   * @param options The options
   * @param args
   * @see canLoad
   * @see unLoad
   * @emits LoadableObject#loading
   * @emits LoadableObject#loaded
   * @emits LoadableObject#loadFailed
   */
  load(options?: loadableObject.LoadOptions, ...args: any[]): Promise<void>;

  /**
   * Check if the instance can be loaded
   * @return {boolean} true if can be loaded, otherwise false
   */
  canLoad(): boolean;

  /**
   * Unload the instance. Usually this is to realise the data
   * @param options The options
   * @param args
   * @see canUnLoad
   * @see load
   * @emits LoadableObject#unLoading
   * @emits LoadableObject#unLoaded
   * @emits LoadableObject#unLoadFailed
   */
  unLoad(options?: loadableObject.UnLoadOptions, ...args: any[]): Promise<void>;

  /**
   * Check if the instance can be unloaded
   * @return {boolean} true if can be unloaded, otherwise false
   */
  canUnLoad(): boolean;

  /**
   * Check if the instance is loaded
   * @return {boolean} true if loaded, otherwise false
   */
  isLoaded(): boolean;
}
