import {BaseInitializableObject, baseInitializableObject} from "./base-initializable-object";
import {runnableObject, RunnableObject} from "./runnable-object";
import {common as commonErrors} from 'zrim-errors';
import * as _ from 'lodash';


export namespace baseRunnableObject {

  export interface ConstructorOptions extends baseInitializableObject.ConstructorOptions {

  }

  export interface Properties extends baseInitializableObject.Properties {

  }
}

const internalSignalNames = _.concat([
  'starting',
  'startFailed',
  'started',
  'paused',
  'pausing',
  'pauseFailed',
  'resuming',
  'resumed',
  'resumeFailed',
  'stopping',
  'stopFailed',
  'stopped',
  'finished',
  'finishedWithError'
], BaseInitializableObject.getSignalNames());

const internalStateNames = _.concat([
  'starting',
  'running',
  'pausing',
  'paused',
  'resuming',
  'stopping',
  'finished'
], BaseInitializableObject.getStateNames());

export class BaseRunnableObject extends BaseInitializableObject implements RunnableObject {

  /**
   * Returns the state names known by the class
   * @return {string[]} The state names
   */
  public static getStateNames(): string[] {
    return internalStateNames;
  }

  /**
   * Returns the signal names the class may emit
   * @return {string[]} The signal names
   */
  public static getSignalNames(): string[] {
    return internalSignalNames;
  }

  /**
   * @inheritDoc
   */
  protected properties: baseRunnableObject.Properties;

  constructor(options?: baseRunnableObject.ConstructorOptions, ...args: any[]) {
    super(options, ...args);
  }

  public async start(options?: runnableObject.StartOptions, ...args: any[]): Promise<void> {
    const logger = this.logger.of({prefixes: ['start']});

    if (!this.canStart()) {
      const names = this.getActivatedStateNames().join(`,`);
      logger.debug(`Cannot start. Active states are '${names}'`);
      throw new commonErrors.IllegalStateError(`Cannot start. Active states are '${names}'`);
    }

    const currentStateName = this.currentStateName;
    this.currentStateName = 'starting';
    this.emit('starting', {_origin: this});
    try {
      logger.debug("Call the start handler");
      await this._handleStart(options, ...args, {
        logger
      });
      logger.debug("Start finished with success");
      this.emit('started', {_origin: this});
      this._addActivatedStateName('running');
      this._removeActivatedStateName('finished');
      this.currentStateName = 'running';
    } catch (error) {
      logger.debug({error}, `Failed to start: ${error.message}`);
      this.emit('startFailed', {
        _origin: this,
        error
      });
      this.currentStateName = currentStateName;
      throw error;
    }
  }

  /**
   * @inheritDoc
   */
  public canStart(): boolean {
    let flag = (this.isStateActivated('initialized') || this.isStateActivated('finished'));
    ['pausing', 'paused', 'resuming', 'running'].forEach(name => {
      flag = flag && !this.isStateActivated(name);
    });

    return !!flag;
  }

  /**
   * @inheritDoc
   */
  public async stop(options?: runnableObject.StopOptions, ...args: any[]): Promise<void> {
    const logger = this.logger.of({prefixes: ['stop']});

    if (!this.canStop()) {
      const names = this.getActivatedStateNames().join(`,`);
      logger.debug(`Cannot stop. Active states are '${names}'`);
      throw new commonErrors.IllegalStateError(`Cannot stop. Active states are '${names}'`);
    }

    const currentStateName = this.currentStateName;
    this.currentStateName = 'stopping';
    this.emit('stopping', {_origin: this});
    try {
      logger.debug("Call the stop handler");
      await this._handleStop(options, ...args, {
        logger
      });
      logger.debug("Stop finished with success");
      this.emit('stopped', {_origin: this});
      this._removeActivatedStateName('running');
      this._removeActivatedStateName('paused');
      this.currentStateName = 'initialized'; // TODO May keep the previous state and set it
    } catch (error) {
      logger.debug({error}, `Failed to stop: ${error.message}`);
      this.emit('stopFailed', {
        _origin: this,
        error
      });
      this.currentStateName = currentStateName;
      throw error;
    }
  }

  /**
   * @inheritDoc
   */
  public canStop(): boolean {
    let flag = false;
    ['paused', 'running'].forEach(name => {
      flag = this.isStateActivated(name) || flag;
    });

    return !!flag;
  }

  /**
   * @inheritDoc
   */
  public isRunning(): boolean {
    return this.isStateActivated('running');
  }

  /**
   * @inheritDoc
   */
  public isPaused(): boolean {
    return this.isStateActivated('paused');
  }

  /**
   * @inheritDoc
   */
  public async pause(options?: runnableObject.PauseOptions, ...args: any[]): Promise<void> {
    const logger = this.logger.of({prefixes: ['pause']});

    if (!this.canPause()) {
      const names = this.getActivatedStateNames().join(`,`);
      logger.debug(`Cannot pause. Active states are '${names}'`);
      throw new commonErrors.IllegalStateError(`Cannot pause. Active states are '${names}'`);
    }

    const currentStateName = this.currentStateName;
    this.currentStateName = 'pausing';
    this.emit('pausing', {_origin: this});
    try {
      logger.debug("Call the pause handler");
      await this._handlePause(options, ...args, {
        logger
      });
      logger.debug("Pause finished with success");
      this.emit('paused', {_origin: this});
      this._addActivatedStateName('paused');
      this._removeActivatedStateName('running');
      this.currentStateName = 'paused';
    } catch (error) {
      logger.debug({error}, `Failed to pause: ${error.message}`);
      this.emit('pauseFailed', {
        _origin: this,
        error
      });
      this.currentStateName = currentStateName;
      throw error;
    }
  }

  /**
   * @inheritDoc
   */
  public canPause(): boolean {
    return this.isPauseAuthorized() && this.isStateActivated('running');
  }

  /**
   * @inheritDoc
   */
  public isPauseAuthorized(): boolean {
    return false;
  }

  /**
   * @inheritDoc
   */
  public async resume(options?: runnableObject.ResumeOptions, ...args: any[]): Promise<void> {
    const logger = this.logger.of({prefixes: ['resume']});

    if (!this.canResume()) {
      const names = this.getActivatedStateNames().join(`,`);
      logger.debug(`Cannot resume. Active states are '${names}'`);
      throw new commonErrors.IllegalStateError(`Cannot resume. Active states are '${names}'`);
    }

    const currentStateName = this.currentStateName;
    this.currentStateName = 'resuming';
    this.emit('resuming', {_origin: this});
    try {
      logger.debug("Call the resume handler");
      await this._handleResume(options, ...args, {
        logger
      });
      logger.debug("Resume finished with success");
      this.emit('resumed', {_origin: this});
      this._addActivatedStateName('running');
      this._removeActivatedStateName('paused');
      this.currentStateName = 'running';
    } catch (error) {
      logger.debug({error}, `Failed to resume: ${error.message}`);
      this.emit('resumeFailed', {
        _origin: this,
        error
      });
      this.currentStateName = currentStateName;
      throw error;
    }
  }

  /**
   * @inheritDoc
   */
  public canResume(): boolean {
    return this.isStateActivated('paused');
  }

  /**
   * Override this method to add your implementation
   * @param options Then options
   * @param args
   */
  protected async _handleStart(options?: runnableObject.StartOptions, ...args: any[]): Promise<void> {}

  /**
   * Override this method to add your implementation
   * @param options Then options
   * @param args
   */
  protected async _handleStop(options?: runnableObject.StopOptions, ...args: any[]): Promise<void> {}

  /**
   * Override this method to add your implementation
   * @param  options Then options
   * @param args
   */
  protected async _handlePause(options?: runnableObject.PauseOptions, ...args: any[]): Promise<void> {}


  /**
   * Override this method to add your implementation
   * @param options Then options
   * @param args
   */
  protected async _handleResume(options?: runnableObject.ResumeOptions, ...args: any[]): Promise<void> {}

  /**
   * Call this method when the execution finished
   * @param {*} result The result to emit
   * @param args The args
   * @emits RunnableObject#finished
   */
  protected async _handleRunDone(result: any, ...args: any[]): Promise<void> {
    const logger = this.logger.of({prefixes: ['_handleRunDone']});

    logger.debug("Run finished with success");
    this._removeActivatedStateName('running');
    this._addActivatedStateName('finished');
    this.currentStateName = 'finished';
    this.emit('finished', {
      _origin: this,
      result
    });
  }

  /**
   * Call this method when the execution finished with an error
   * @param {Error} error The error
   * @param args
   * @return {Promise<void>}
   * @emits RunnableObject#finishedWithError
   */
  protected async _handleExecutionFailed(error: Error, ...args: any[]): Promise<void> {
    const logger = this.logger.of({prefixes: ['_handleExecutionFailed']});

    logger.debug({error}, `Run finished with error: ${error.message}`);
    this._removeActivatedStateName('running');
    this.currentStateName = 'finishing';
    this.emit('finishedWithError', {
      _origin: this,
      error
    });
    this._addActivatedStateName('finished');
    this.currentStateName = 'finished'; // TODO : Change for original state
  }

  /**
   * @inheritDoc
   */
  protected async _handleFinalization(...args: any[]) {
    const logger = args[args.length - 1].logger.of({prefixes: ['_handleFinalization']});

    logger.debug("Check if can stop it");
    if (this.canStop()) {
      logger.debug("Stop the instance");
      await this.stop(...args);
      logger.debug("Stop finished with success");
    }

    logger.debug("Call the super._handleFinalization");
    return await super._handleFinalization(...args);
  }

  /**
   * @inheritDoc
   */
  protected _acceptStateName(value: string): boolean {
    return BaseRunnableObject.getStateNames().indexOf(value) >= 0;
  }
}
