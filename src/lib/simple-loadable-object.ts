import {BaseLoadableObject} from "./base-loadable-object";

/**
 * A simple loadable object. It automatically pass from loaded to ready
 */
export class SimpleLoadableObject extends BaseLoadableObject {

  /**
   * @inheritDoc
   */
  protected _getStateNamesForReady(): string[] | undefined {
    return ['loaded'];
  }
}
