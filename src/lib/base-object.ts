import {EventEmitter} from "events";
import * as proxyLoggerLib from "zrim-proxy-logger";
import {ZrimObject} from "./zrim-object";
import * as utils from 'zrim-utils';
import {TargetLogger} from "zrim-proxy-logger/dist/lib/target-logger";
import * as _ from 'lodash';

export namespace baseObject {

  export interface ConstructorOptions {
    /**
     * The target logger to use with the proxy
     */
    targetLogger?: TargetLogger;
  }

  export interface ActiveState {
    activatedAt: Date;
  }

  /**
   * Internal properties container
   */
  export interface Properties {
    /**
     * Contains the activated states. The key is the state name
     */
    activatedStates: {[key: string]: ActiveState};
    /**
     * Contains the current state name
     */
    currentStateName: string;
    /**
     * Contains the instance class name
     */
    __className: string;
    /**
     * Ignore emit call or not
     */
    ignoreSignals: boolean;
  }
}

const internalSignalNames = [
  'ready',
  'currentStateChanged',
  'ignoreSignalsChanged',
  'flagChanged',
  'propertyChanged'
];

const internalStateNames = [
  'none',
  'ready'
];


/**
 * Simple base object.
 * Contains base utilities
 */
export class BaseObject extends EventEmitter implements ZrimObject {

  /**
   * Returns the state names known by the class
   * @return The state names
   */
  public static getStateNames(): string[] {
    return internalStateNames;
  }

  /**
   * Returns the signal names the class may emit
   * @return The signal names
   */
  public static getSignalNames(): string[] {
    return internalSignalNames;
  }

  /**
   * @inheritDoc
   */
  public readonly logger: proxyLoggerLib.ProxyLogger;

  /**
   * Internal properties
   */
  protected properties: baseObject.Properties;

  constructor(options?: baseObject.ConstructorOptions, ...args: any[]) {
    super();

    const myClassName = utils.classUtils.getClassName(this);

    Object.defineProperty(this, "properties", {
      value: {
        activatedStates: {}, // Contains the states activated
        currentStateName: 'none',
        __className: myClassName,
        ignoreSignals: false
      },
      enumerable: false,
      writable: true
    });

    Object.defineProperty(this, 'logger', {
      value: this._newProxyLogger(...args),
      enumerable: false,
      writable: true
    });

    // Call the init function
    this._initFromConstructor(options, ...args);
  }

  /**
   * @inheritDoc
   */
  public get __className(): string {
    return this.properties.__className;
  }

  /**
   * @inheritDoc
   */
  public get currentStateName(): string {
    return this.properties.currentStateName;
  }

  /**
   * @inheritDoc
   * @param value
   */
  public set currentStateName(value: string) {
    const currentStateName = this.currentStateName;

    if (currentStateName === value || this._acceptStateName(value) !== true) {
      return; // Nothing change
    }

    this.logger.debug("Change state from '%s' to '%s'", currentStateName, value);
    this.properties.currentStateName = value;
    this._emitPropertyChanged('currentStateName', value, currentStateName);
  }

  /**
   * @inheritDoc
   */
  public get ignoreSignals(): boolean {
    return this.properties.ignoreSignals === true;
  }

  /**
   * @inheritDoc
   * @param value
   */
  public set ignoreSignals(value: boolean) {
    const currentValue = this.ignoreSignals;
    if (currentValue === value || typeof value !== 'boolean') {
      return; // Nothing change
    }

    this.logger.debug("Change flags 'ignoreSignals' from '%s' to '%s'", currentValue, value);
    this.properties.ignoreSignals = value;
    const event = {
      _origin: this,
      previousValue: currentValue,
      newValue: value
    };
    this._emit('ignoreSignalsChanged', event);
    this._emitFlagChanged("ignoreSignals", value, currentValue);
  }

  /**
   * @inheritDoc
   */
  public emit(event: string, ...args: any[]): boolean {
    if (this.ignoreSignals === true) {
      return;
    }

    let result;
    try {
      result = super.emit(event, ...args);
    } catch (error) {
      this.logger.debug(`Catch error from emitting '${event}'`);
    }

    return result;
  }

  /**
   * @inheritDoc
   */
  public getActivatedStateNames(): string[] {
    return Object.keys(this.properties.activatedStates);
  }

  /**
   * @inheritDoc
   */
  public isReady(): boolean {
    return this.isStateActivated('ready');
  }

  /**
   * @inheritDoc
   */
  public isStateActivated(name: string): boolean {
    return !!this.properties.activatedStates[name];
  }


  /**
   * This methods is called by the constructor to initialize the data
   * @param args
   */
  protected _initFromConstructor(options?: baseObject.ConstructorOptions, ...args: any[]) {
    if (_.isObject(options)) {
      const targetLogger = options.targetLogger;
      if (_.isObject(targetLogger)) {
        this.logger.target = targetLogger;
      }
    }
  }

  /**
   * Create a new proxy logger for the object
   * @param args The argument receive from the constructor
   */
  protected _newProxyLogger(...args: any[]): proxyLoggerLib.ProxyLogger {
    return new proxyLoggerLib.SimpleGenericProxyLogger({
      target: proxyLoggerLib.defaultLoggerManagerHandler.getLogger(this.properties.__className),
      prefixes: [this.properties.__className]
    });
  }

  /**
   * Force to emit event even if the signal is disabled
   * @param {string} eventName The name
   * @param args
   */
  protected _emit(event: string, ...args: any[]) {
    return EventEmitter.prototype.emit.call(this, event, ...args);
  }

  /**
   * Emit the signal for a property changed
   * @param {string} name The property name
   * @param {*} newValue The new value
   * @param {*} previousValue The previous value
   * @emits ZrimObject#propertyChanged
   */
  protected _emitPropertyChanged(name: string, newValue: any, previousValue: any) {
    this.emit('propertyChanged', {
      _origin: this,
      name, newValue, previousValue
    });
    this.emit(`${name}Changed`, {
      _origin: this,
      newValue, previousValue
    });
  }

  /**
   * Emit a change of state
   * @param newState
   * @param currentStateName
   * @emits ZrimObject#currentStateChanged
   */
  protected _emitCurrentStateChanged(newState: string, currentStateName: string | undefined) {
    this.emit('currentStateChanged', {
      _origin: this,
      newValue: newState,
      previousValue: currentStateName
    });
  }

  /**
   * Emit the signal to indicate a internal flag value changed
   * @param {string} name The flag name
   * @param {*} newValue The new value
   * @param {*} previousValue The previous value
   * @emits ZrimObject#flagChanged
   */
  protected _emitFlagChanged(name: string, newValue: any, previousValue: any) {
    return this._emit('flagChanged', {
      _origin: this,
      name, previousValue, newValue
    }); // Force to send the event
  }

  /**
   * Add a state name to the activated list
   * @param {string} name The state name
   * @emits ZrimObject#stateActivated
   * @see _removeActivatedStateName
   */
  protected _addActivatedStateName(name: string) {
    if (typeof name !== 'string' || !name) {
      throw new TypeError(`Invalid name`);
    }

    this.properties.activatedStates[name] = {
      activatedAt: new Date(Date.now())
    };
    this._emit('stateActivated', {
      _origin: this,
      stateName: name
    });
    this._updateReadyState();
  }

  /**
   * Remove a state name to the activated list
   * @param {string} name The state name
   * @emits ZrimObject#stateDeactivated
   * @see _addActivatedStateName
   */
  protected _removeActivatedStateName(name: string) {
    if (typeof name !== 'string' || !name) {
      throw new TypeError(`Invalid name`);
    }

    delete this.properties.activatedStates[name];
    this._emit('stateDeactivated', {
      _origin: this,
      stateName: name
    });
    this._updateReadyState();
  }

  /**
   * Function to test if the state is accepted
   * @param name The state name
   * @return true if the state name is accepted, otherwise false
   */
  protected _acceptStateName(name: string): boolean {
    return BaseObject.getStateNames().indexOf(name) >= 0;
  }

  /**
   * Returns the state required to be ready
   * @return undefined to disable the feature, otherwise the state names
   */
  protected _getStateNamesForReady(): string[] | undefined {
    return undefined;
  }

  /**
   * Call to update the ready state
   */
  protected _updateReadyState() {
    const requireStateNames = this._getStateNamesForReady();

    if (!requireStateNames) {
      return; // Not enabled
    }

    const readyEnabled = this.isReady();
    let ready = true;
    requireStateNames.forEach(stateName => {
      ready = ready && this.isStateActivated(stateName);
    });

    if (ready !== readyEnabled) {
      // Changed state
      if (ready) {
        this._addActivatedStateName('ready');
      } else {
        this._removeActivatedStateName('ready');
      }
    }
  }
}
