import {BaseRunnableObject} from "./base-runnable-object";

/**
 * A simple runnable object. It automatically pass from initialized to ready
 */
export class SimpleRunnableObject extends BaseRunnableObject {

  /**
   * @inheritDoc
   */
  protected _getStateNamesForReady(): string[] | undefined {
    return ['initialized'];
  }
}
