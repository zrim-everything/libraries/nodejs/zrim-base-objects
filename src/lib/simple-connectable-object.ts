import {BaseConnectableObject} from "./base-connectable-object";

/**
 * A simple connectable object. It automatically pass from connected to ready
 */
export class SimpleConnectableObject extends BaseConnectableObject {

  /**
   * @inheritDoc
   */
  protected _getStateNamesForReady(): string[] | undefined {
    return ['connected'];
  }
}
