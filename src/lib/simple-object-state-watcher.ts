import {SimpleRunnableObject} from "./simple-runnable-object";
import {baseRunnableObject} from "./base-runnable-object";
import {zrimObject, ZrimObject} from "./zrim-object";
import {objectStateWatcher, ObjectStateWatcher} from "./object-state-watcher";
import * as _ from 'lodash';

export namespace simpleObjectStateWatcher {

  export interface ConstructorOptions extends baseRunnableObject.ConstructorOptions {

  }

  export interface ObjectDescription {
    /**
     * The object to check
     */
    object: ZrimObject;

    /**
     * The activated state names of the object
     */
    activatedStateNames: string[];

    /**
     * The handler that receive the events : stateActivated
     */
    onStateActivated: (...args: any[]) => void;

    /**
     * The handler that receive the events : stateDeactivated
     */
    onStateDeactivated: (...args: any[]) => void;
  }

  export interface Properties extends baseRunnableObject.Properties {
    /**
     * The state name to wait
     */
    waitingStateName: string;

    /**
     * If the state are synchronized
     */
    synchronized: boolean;

    /**
     * Contains the object descriptions
     */
    objectDescriptions: ObjectDescription[];
  }
}

const internalSignalNames = _.concat([
  'synchronized',
  'desynchronized'
], SimpleRunnableObject.getSignalNames());

export class SimpleObjectStateWatcher extends SimpleRunnableObject implements ObjectStateWatcher {

  /**
   * Returns the signal names the class may emit
   * @return {string[]} The signal names
   */
  public static getSignalNames(): string[] {
    return internalSignalNames;
  }


  /**
   * @inheritDoc
   */
  protected properties: simpleObjectStateWatcher.Properties;

  constructor(options?: simpleObjectStateWatcher.ConstructorOptions, ...args: any[]) {
    super(options, ...args);

    this.properties.waitingStateName = "ready";
    this.properties.synchronized = false;
    this.properties.objectDescriptions = [];
  }

  /**
   * @inheritDoc
   */
  public async initialize(options?: objectStateWatcher.InitializeOptions, ...args: any[]): Promise<void> {
    await super.initialize(options, ...args);
  }

  /**
   * @inheritDoc
   */
  public isSynchronized(): boolean {
    return this.properties.synchronized === true;
  }

  /**
   * @inheritDoc
   */
  protected async _handleInitialization(options?: objectStateWatcher.InitializeOptions, ...args: any[]): Promise<void> {
    await super._handleInitialization(options, ...args);

    this.properties.objectDescriptions = options.objects.map(item => {
      return {
        object: item,
        activatedStateNames: [],
        onStateActivated: this._onStateActivated.bind(this),
        onStateDeactivated: this._onStateDeactivated.bind(this)
      };
    });
    this.properties.waitingStateName = options.waitingStateName;
  }

  /**
   * Handle the event from BaseObject#stateActivated
   * @param event The event
   */
  protected _onStateActivated(event: zrimObject.events.StateActivated): void {
    this.logger.debug({prefixes: ['_onStateActivated']}, `Received state activated for state '${event.stateName}'`);
    const description = _.find(this.properties.objectDescriptions, {
      object: event._origin
    });

    if (description) {
      this.logger.debug({prefixes: ['_onStateActivated']}, "Update the description");
      description.activatedStateNames.push(event.stateName);
      this._updateSynchronizedState();
    }
  }

  /**
   * Handle the event from BaseObject#stateDeactivated
   * @param event The event
   */
  protected _onStateDeactivated(event: zrimObject.events.StateDeactivated): void {
    this.logger.debug({prefixes: ['_onStateDeactivated']}, `Received state deactivated for state '${event.stateName}'`);
    const description = _.find(this.properties.objectDescriptions, {
      object: event._origin
    });

    if (description) {
      this.logger.debug({prefixes: ['_onStateDeactivated']}, "Update the description");
      _.remove(description.activatedStateNames, item => item === event.stateName);
      this._updateSynchronizedState();
    }
  }

  /**
   * Synchronize the state to know if object are in sync
   */
  protected _updateSynchronizedState(): void {
    const objects = _.filter(this.properties.objectDescriptions, item => {
      return item.activatedStateNames.indexOf(this.properties.waitingStateName) >= 0;
    });

    const synchronized = objects.length === this.properties.objectDescriptions.length;

    // Check if the value changed
    if (synchronized === this.properties.synchronized) {
      // No change
      return;
    }

    this.properties.synchronized = synchronized;
    this.emit(synchronized ? 'synchronized' : 'desynchronized', {
      _origin: this
    });
  }

  /**
   * @inheritDoc
   */
  protected async _handleStart(...args: any[]) {
    /* eslint-enable no-unused-vars */
    this.logger.debug({prefixes: ['_handleStart']}, "Listen to events");

    this.properties.objectDescriptions.forEach(item => {
      item.object.addListener('stateActivated', item.onStateActivated);
      item.object.addListener('stateDeactivated', item.onStateDeactivated);
      item.activatedStateNames = _.concat([], item.object.getActivatedStateNames()); // We clone it
    });

    this.logger.debug({prefixes: ['_handleStart']}, "Force to sync the states");
    this._updateSynchronizedState();
  }

  /**
   * @inheritDoc
   */
  protected async _handleStop(...args: any[]) {
    /* eslint-enable no-unused-vars */
    this.logger.debug({prefixes: ['_handleStop']}, "Remove listener");

    this.properties.objectDescriptions.forEach(item => {
      item.object.removeListener('stateActivated', item.onStateActivated);
      item.object.removeListener('stateDeactivated', item.onStateDeactivated);
    });
  }

  /**
   * @inheritDoc
   */
  protected async _handleResume(...args: any[]) {
    this.logger.debug({prefixes: ['_handleResume']}, "Call the _handleStart");
    await this._handleStart(...args);
  }

  /**
   * @inheritDoc
   */
  protected async _handlePause(...args: any[]) {
    this.logger.debug({prefixes: ['_handlePause']}, "Call the _handleStop");
    await this._handleStop(...args);
  }

  /**
   * @inheritDoc
   */
  protected _acceptStateName(value: string): boolean {
    return SimpleObjectStateWatcher.getStateNames().indexOf(value) >= 0;
  }
}
