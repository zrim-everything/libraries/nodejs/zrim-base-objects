import {RunnableObject} from "./runnable-object";
import {initializableObject} from "./initializable-object";
import {ZrimObject, zrimObject} from "./zrim-object";

/**
 * All object are synchronized
 *
 * @event SimpleObjectStateWatcher#synchronized
 * @type {object}
 * @property {SimpleObjectStateWatcher} _origin The origin instance
 */
/**
 * One or more object are desynchronized
 *
 * @event SimpleObjectStateWatcher#desynchronized
 * @type {object}
 * @property {SimpleObjectStateWatcher} _origin The origin instance
 */

export namespace objectStateWatcher {

  export interface InitializeOptions extends initializableObject.InitializeOptions {
    /**
     * The objects to wait on
     */
    objects: ZrimObject[];

    /**
     * Which state name to watch
     */
    waitingStateName: string;
  }


  export namespace events {

    /**
     * Signal: synchronized
     */
    export interface Synchronized extends zrimObject.events.BaseEvent<ObjectStateWatcher> {

    }

    /**
     * Signal: desynchronized
     */
    export interface Desynchronized extends zrimObject.events.BaseEvent<ObjectStateWatcher> {

    }
  }
}

export interface ObjectStateWatcher extends RunnableObject {

  /**
   * @inheritDoc
   */
  initialize(options?: objectStateWatcher.InitializeOptions, ...args: any[]): Promise<void>;

  /**
   * Indicate if the object are synchronized
   */
  isSynchronized(): boolean;
}
