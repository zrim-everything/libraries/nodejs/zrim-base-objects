import {LoadableObject, loadableObject} from "./loadable-object";
import {baseInitializableObject, BaseInitializableObject} from "./base-initializable-object";
import {common as commonErrors} from 'zrim-errors';
import * as _ from 'lodash';

export namespace baseLoadableObject {

  export interface ConstructorOptions extends baseInitializableObject.ConstructorOptions {

  }

  export interface Properties extends baseInitializableObject.Properties {

  }
}

const internalSignalNames = _.concat([
  'loading',
  'loadFailed',
  'loaded',
  'unLoading',
  'unLoadFailed',
  'unLoaded'
], BaseInitializableObject.getSignalNames());

const internalStateNames = _.concat([
  'loading',
  'unLoading',
  'loaded'
], BaseInitializableObject.getStateNames());

export class BaseLoadableObject extends BaseInitializableObject implements LoadableObject {

  /**
   * Returns the state names known by the class
   * @return {string[]} The state names
   */
  public static getStateNames(): string[] {
    return internalStateNames;
  }

  /**
   * Returns the signal names the class may emit
   * @return {string[]} The signal names
   */
  public static getSignalNames(): string[] {
    return internalSignalNames;
  }

  /**
   * @inheritDoc
   */
  protected properties: baseLoadableObject.Properties;

  constructor(options?: baseLoadableObject.ConstructorOptions, ...args: any[]) {
    super(options, ...args);
  }

  /**
   * @inheritDoc
   */
  public async load(options?: loadableObject.LoadOptions, ...args: any[]): Promise<void> {
    const logger = this.logger.of({prefixes: ['load']});

    if (!this.canLoad()) {
      const names = this.getActivatedStateNames().join(`,`);
      logger.debug(`Cannot load. Active states are '${names}'`);
      throw new commonErrors.IllegalStateError(`Cannot load. Active states are '${names}'`);
    }

    const currentStateName = this.currentStateName;
    this.currentStateName = 'loading';
    this.emit('loading', {_origin: this});
    try {
      logger.debug("Call the load handler");
      await this._handleLoad(options, ...args, {
        logger
      });
      logger.debug("Load finished with success");
      this.emit('loaded', {_origin: this});
      this._addActivatedStateName('loaded');
      this.currentStateName = 'loaded';
    } catch (error) {
      logger.debug({error}, `Failed to load: ${error.message}`);
      this.emit('loadFailed', {
        _origin: this,
        error
      });
      this.currentStateName = currentStateName;
      throw error;
    }
  }

  /**
   * @inheritDoc
   */
  public canLoad(): boolean {
    return this.isStateActivated('initialized') && !this.isStateActivated('loaded');
  }

  /**
   * @inheritDoc
   */
  public async unLoad(options?: loadableObject.UnLoadOptions, ...args: any[]): Promise<void> {
    const logger = this.logger.of({prefixes: ['unLoad']});

    if (!this.canUnLoad()) {
      const names = this.getActivatedStateNames().join(`,`);
      logger.debug(`Cannot unload. Active states are '${names}'`);
      throw new commonErrors.IllegalStateError(`Cannot unload. Active states are '${names}'`);
    }

    const currentStateName = this.currentStateName;
    this.currentStateName = 'unLoading';
    this.emit('unLoading', {_origin: this});
    this._removeActivatedStateName('loaded');
    try {
      logger.debug("Call the unLoad handler");
      await this._handleUnLoad(options, ...args, {
        logger
      });
      logger.debug("UnLoad finished with success");
      this.emit('unLoaded', {_origin: this});
      this.currentStateName = 'initialized'; // TODO May keep the previous state and set it
    } catch (error) {
      logger.debug({error}, `Failed to unload: ${error.message}`);
      this.emit('unLoadFailed', {
        _origin: this,
        error
      });
      this.currentStateName = currentStateName;
      this._addActivatedStateName('loaded');
      throw error;
    }
  }

  /**
   * @inheritDoc
   */
  public canUnLoad(): boolean {
    return this.isStateActivated('loaded');
  }


  /**
   * @inheritDoc
   */
  public isLoaded(): boolean {
    return this.isStateActivated('loaded');
  }

  /**
   * Override this method to add your implementation
   * @param options Then options
   * @param args
   */
  protected async _handleLoad(options?: loadableObject.LoadOptions, ...args: any[]): Promise<void> {}

  /**
   * Override this method to add your implementation
   * @param options Then options
   * @param args
   */
  protected async _handleUnLoad(options?: loadableObject.UnLoadOptions, ...args: any[]): Promise<void> {}

  /**
   * @inheritDoc
   */
  protected async _handleFinalization(...args: any[]) {
    const logger = args[args.length - 1].logger.of({prefixes: ['_handleFinalization']});
    logger.debug("Check if the instance is loaded");
    if (this.isLoaded()) {
      logger.debug("Unload the instance");
      await this.unLoad(...args);
      logger.debug("Unload finished with success");
    }

    logger.debug("Call the super._handleFinalization");
    return await super._handleFinalization(...args);
  }

  /**
   * @inheritDoc
   */
  protected _acceptStateName(value: string): boolean {
    return BaseLoadableObject.getStateNames().indexOf(value) >= 0;
  }
}
