import {BaseObject, baseObject} from "./base-object";
import {InitializableObject, initializableObject} from "./initializable-object";
import {common as commonErrors} from 'zrim-errors';
import * as _ from 'lodash';

export namespace baseInitializableObject {

  export interface ConstructorOptions extends baseObject.ConstructorOptions {

  }

  export interface Properties extends baseObject.Properties {

  }
}

const internalSignalNames = _.concat([
  'initializing',
  'initializationSucceed',
  'initializationFailed',
  'finalizing',
  'finalizationSucceed',
  'finalizationFailed'
], BaseObject.getSignalNames());

const internalStateNames = _.concat([
  'notInitialized',
  'initializing',
  'initialized',
  'finalizing'
], BaseObject.getStateNames());

export class BaseInitializableObject extends BaseObject implements InitializableObject {

  /**
   * Returns the state names known by the class
   * @return {string[]} The state names
   */
  public static getStateNames(): string[] {
    return internalStateNames;
  }

  /**
   * Returns the signal names the class may emit
   * @return {string[]} The signal names
   */
  public static getSignalNames(): string[] {
    return internalSignalNames;
  }

  /**
   * @inheritDoc
   */
  protected properties: baseInitializableObject.Properties;

  constructor(options?: baseInitializableObject.ConstructorOptions, ...args: any[]) {
    super(options, ...args);

    this.properties.currentStateName = 'notInitialized';
    this._addActivatedStateName('notInitialized');
  }

  public async initialize(options?: initializableObject.InitializeOptions, ...args: any[]): Promise<void> {
    const logger = this.logger.of({prefixes: ['initialize']});

    if (!this.canInitialize()) {
      const names = this.getActivatedStateNames().join(`,`);
      logger.debug(`Cannot initialize. Active states are '${names}'`);
      throw new commonErrors.IllegalStateError(`Cannot initialize. Active states are '${names}'`);
    }

    const currentStateName = this.currentStateName;
    this.currentStateName = 'initializing';
    this.emit('initializing', {_origin: this});
    try {
      logger.debug("Call the initialization handler");
      await this._handleInitialization(options, ...args, {
        logger
      });
      logger.debug("Initialization finished with success");
      this.emit('initializationSucceed', {_origin: this});
      this._addActivatedStateName('initialized');
      this.currentStateName = 'initialized';
      this._removeActivatedStateName('notInitialized');
    } catch (error) {
      logger.debug({error}, `Failed to initialize: ${error.message}`);
      this.emit('initializationFailed', {
        _origin: this,
        error
      });
      this.currentStateName = currentStateName;
      throw error;
    }
  }

  /**
   * @inheritDoc
   */
  public canInitialize(): boolean {
    return this.isStateActivated('notInitialized');
  }

  /**
   * @inheritDoc
   */
  public isInitialized(): boolean {
    return this.isStateActivated('initialized');
  }

  /**
   * @inheritDoc
   */
  public async finalize(options?: initializableObject.FinalizeOptions, ...args: any[]): Promise<void> {
    const logger = this.logger.of({prefixes: ['finalize']});

    if (!this.canFinalize()) {
      const names = this.getActivatedStateNames().join(',');
      logger.debug(`Cannot finalize. Active states are '${names}'`);
      throw new commonErrors.IllegalStateError(`Cannot finalize. Active states are '${names}'`);
    }

    const currentStateName = this.currentStateName;
    this.currentStateName = 'finalizing';
    this.emit('finalizing', {_origin: this});
    this._removeActivatedStateName('initialized');
    try {
      logger.debug("Call the finalization handler");
      await this._handleFinalization(options, ...args, {
        logger
      });
      logger.debug("Finalization finished with success");
      this.emit('finalizationSucceed', {_origin: this});
      this._addActivatedStateName('notInitialized');
      this.currentStateName = 'notInitialized';
    } catch (error) {
      logger.debug({error}, `Failed to finalize: ${error.message}`);
      this.emit('finalizationFailed', {
        _origin: this,
        error
      });
      this.currentStateName = currentStateName;
      this._addActivatedStateName('initialized');
      throw error;
    }
  }

  /**
   * @inheritDoc
   */
  public canFinalize(): boolean {
    return this.isStateActivated('initialized');
  }

  /**
   * Override this method to add your implementation
   * @param options
   * @param args
   */
  protected async _handleInitialization(options?: initializableObject.InitializeOptions, ...args: any[]): Promise<void> {}

  /**
   * Override this method to add your implementation
   * @param options
   * @param args
   */
  protected async _handleFinalization(options?: initializableObject.FinalizeOptions, ...args: any[]): Promise<void> {}

  /**
   * @inheritDoc
   */
  protected _acceptStateName(value) {
    return BaseInitializableObject.getStateNames().indexOf(value) >= 0;
  }
}
