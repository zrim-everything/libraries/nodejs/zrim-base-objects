import {ConnectableObject, connectableObject} from "./connectable-object";
import {BaseInitializableObject, baseInitializableObject} from "./base-initializable-object";
import {common as commonErrors} from 'zrim-errors';
import * as _ from 'lodash';

export namespace baseConnectableObject {

  export interface ConstructorOptions extends baseInitializableObject.ConstructorOptions {

  }

  export interface Properties extends baseInitializableObject.Properties {

  }
}

const internalSignalNames = _.concat([
  'connecting',
  'connectionFailed',
  'connected',
  'connectionLost',
  'reconnected',
  'disconnecting',
  'disconnectionFailed',
  'disconnected'
], BaseInitializableObject.getSignalNames());

const internalStateNames = _.concat([
  'connecting',
  'disconnecting',
  'connected'
], BaseInitializableObject.getStateNames());

/**
 * A connectable object
 */
export class BaseConnectableObject extends BaseInitializableObject implements ConnectableObject {

  /**
   * Returns the state names known by the class
   * @return {string[]} The state names
   */
  public static getStateNames(): string[] {
    return internalStateNames;
  }

  /**
   * Returns the signal names the class may emit
   * @return {string[]} The signal names
   */
  public static getSignalNames(): string[] {
    return internalSignalNames;
  }

  /**
   * @inheritDoc
   */
  protected properties: baseConnectableObject.Properties;

  constructor(options?: baseConnectableObject.ConstructorOptions, ...args: any[]) {
    super(options, ...args);
  }


  /**
   * @inheritDoc
   */
  public async connect(options?: connectableObject.ConnectOptions, ...args: any[]): Promise<void> {
    const logger = this.logger.of({prefixes: ['connect']});

    if (!this.canConnect()) {
      const names = this.getActivatedStateNames().join(`,`);
      logger.debug(`Cannot connect. Active states are '${names}'`);
      throw new commonErrors.IllegalStateError(`Cannot connect. Active states are '${names}'`);
    }

    const currentStateName = this.currentStateName;
    this.currentStateName = 'connecting';
    this.emit('connecting', {_origin: this});
    try {
      logger.debug("Call the connect handler");
      await this._handleConnection(options, ...args, {
        logger
      });
      logger.debug("Connect finished with success");
      this.emit('connected', {_origin: this});
      this._addActivatedStateName('connected');
      this.currentStateName = 'connected';
    } catch (error) {
      logger.debug({error}, `Failed to connect: ${error.message}`);
      this.emit('connectionFailed', {
        _origin: this,
        error
      });
      this.currentStateName = currentStateName;
      throw error;
    }
  }

  /**
   * @inheritDoc
   */
  public canConnect(): boolean {
    return this.isStateActivated('initialized') && !this.isStateActivated('connected');
  }

  /**
   * @inheritDoc
   */
  public async disconnect(options?: connectableObject.DisconnectOptions, ...args: any[]): Promise<void> {
    const logger = this.logger.of({prefixes: ['disconnect']});

    if (!this.canDisconnect()) {
      const names = this.getActivatedStateNames().join(`,`);
      logger.debug(`Cannot disconnect. Active states are '${names}'`);
      throw new commonErrors.IllegalStateError(`Cannot disconnect. Active states are '${names}'`);
    }

    const currentStateName = this.currentStateName;
    this.currentStateName = 'disconnecting';
    this.emit('disconnecting', {_origin: this});
    this._removeActivatedStateName('connected');
    try {
      logger.debug("Call the disconnect handler");
      await this._handleDisconnection(options, ...args, {
        logger
      });
      logger.debug("disconnect finished with success");
      this.emit('disconnected', {_origin: this});
      this.currentStateName = 'initialized'; // TODO May keep the previous state and set it
    } catch (error) {
      logger.debug({error}, "Failed to disconnect: %s", error.message);
      this.emit('disconnectionFailed', {
        _origin: this,
        error
      });
      this.currentStateName = currentStateName;
      this._addActivatedStateName('connected');
      throw error;
    }
  }

  /**
   * @inheritDoc
   */
  public canDisconnect(): boolean {
    return this.isStateActivated('connected');
  }

  /**
   * @inheritDoc
   */
  public isConnected(): boolean {
    return this.isStateActivated('connected');
  }

  /**
   * Override this method to add your implementation
   * @param options Then options
   * @param args
   */
  protected async _handleDisconnection(options: connectableObject.DisconnectOptions, ...args: any[]): Promise<void> {}

  /**
   * Override this method to add your implementation
   * @param options Then options
   * @param args
   */
  protected async _handleConnection(options: connectableObject.ConnectOptions, ...args: any[]): Promise<void> { }

  /**
   * Call this method to indicate the connection was lost
   */
  protected async _onConnectionLost(): Promise<void> {
    const logger = this.logger.of({prefixes: ['_onConnectionLost']});
    logger.debug("Received connection lost");
    this.currentStateName = 'initialized';
    this._removeActivatedStateName('connected');
    this.emit('connectionLost', {
      _origin: this
    });
  }

  /**
   * Call this method to indicate the instance reconnect
   */
  protected async _onReconnected(): Promise<void> {
    const logger = this.logger.of({prefixes: ['_onReconnected']});
    logger.debug("Receive reconnected");
    this.currentStateName = 'connected';
    this._addActivatedStateName('connected');
    this.emit('reconnected', {
      _origin: this
    });
  }

  /**
   * @inheritDoc
   */
  protected async _handleFinalization(...args: any[]) {
    const logger = args[args.length - 1].logger.of({prefixes: ['_handleFinalization']});
    logger.debug("Check if the instance is connected");
    if (this.isConnected()) {
      logger.debug("disconnect the instance");
      await this.disconnect(...args);
      logger.debug("disconnect finished with success");
    }

    logger.debug("Call the super._handleFinalization");
    return await super._handleFinalization(...args);
  }

  /**
   * @inheritDoc
   */
  protected _acceptStateName(value: string): boolean {
    return BaseConnectableObject.getStateNames().indexOf(value) >= 0;
  }
}
