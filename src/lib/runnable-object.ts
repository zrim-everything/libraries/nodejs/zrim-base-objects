import {
  InitializableObject
} from "./initializable-object";
import {zrimObject} from "./zrim-object";


/**
 * The object is starting
 *
 * @event RunnableObject#starting
 * @type {object}
 * @property {RunnableObject} _origin The origin instance
 */
/**
 * The object started with success
 *
 * @event RunnableObject#started
 * @type {object}
 * @property {RunnableObject} _origin The origin instance
 */
/**
 * The start failed
 *
 * @event RunnableObject#startFailed
 * @type {object}
 * @property {RunnableObject} _origin The origin instance
 * @property {Error} error The error
 */
/**
 * The instance is pausing
 *
 * @event RunnableObject#pausing
 * @type {object}
 * @property {RunnableObject} _origin The origin instance
 */
/**
 * The instance is in paused
 *
 * @event RunnableObject#paused
 * @type {object}
 * @property {RunnableObject} _origin The origin instance
 */
/**
 * The pause failed
 *
 * @event RunnableObject#pauseFailed
 * @type {object}
 * @property {RunnableObject} _origin The origin instance
 * @property {Error} error The error
 */
/**
 * The instance is resuming
 *
 * @event RunnableObject#resuming
 * @type {object}
 * @property {RunnableObject} _origin The origin instance
 */
/**
 * The instance is in resumed
 *
 * @event RunnableObject#resumed
 * @type {object}
 * @property {RunnableObject} _origin The origin instance
 */
/**
 * The resume failed
 *
 * @event RunnableObject#resumeFailed
 * @type {object}
 * @property {RunnableObject} _origin The origin instance
 * @property {Error} error The error
 */
/**
 * The instance is stopping
 *
 * @event RunnableObject#stopping
 * @type {object}
 * @property {RunnableObject} _origin The origin instance
 */
/**
 * The instance stopped
 *
 * @event RunnableObject#stopped
 * @type {object}
 * @property {RunnableObject} _origin The origin instance
 */
/**
 * The stop failed
 *
 * @event RunnableObject#stopFailed
 * @type {object}
 * @property {RunnableObject} _origin The origin instance
 * @property {Error} error The error
 */
/**
 * The instance finished
 *
 * @event RunnableObject#finished
 * @type {object}
 * @property {RunnableObject} _origin The origin instance
 */
/**
 * The finished with an error
 *
 * @event RunnableObject#finishedWithError
 * @type {object}
 * @property {RunnableObject} _origin The origin instance
 * @property {Error} error The error
 */

export namespace runnableObject {

  export interface StartOptions {

  }

  export interface StopOptions {

  }

  export interface PauseOptions {

  }

  export interface ResumeOptions {

  }

  export namespace events {

    /**
     * Signal: starting
     */
    export interface Starting extends zrimObject.events.BaseEvent<RunnableObject> {

    }

    /**
     * Signal: started
     */
    export interface Started extends zrimObject.events.BaseEvent<RunnableObject> {

    }

    /**
     * Signal: startFailed
     */
    export interface StartFailed extends zrimObject.events.BaseEvent<RunnableObject> {

      /**
       * The error
       */
      error: Error;
    }

    /**
     * Signal: pausing
     */
    export interface Pausing extends zrimObject.events.BaseEvent<RunnableObject> {

    }

    /**
     * Signal: paused
     */
    export interface Paused extends zrimObject.events.BaseEvent<RunnableObject> {

    }

    /**
     * Signal: pauseFailed
     */
    export interface PauseFailed extends zrimObject.events.BaseEvent<RunnableObject> {

      /**
       * The error
       */
      error: Error;
    }

    /**
     * Signal: resuming
     */
    export interface Resuming extends zrimObject.events.BaseEvent<RunnableObject> {

    }

    /**
     * Signal: resumed
     */
    export interface Resumed extends zrimObject.events.BaseEvent<RunnableObject> {

    }

    /**
     * Signal: resumeFailed
     */
    export interface ResumeFailed extends zrimObject.events.BaseEvent<RunnableObject> {

      /**
       * The error
       */
      error: Error;
    }

    /**
     * Signal: stopping
     */
    export interface Stopping extends zrimObject.events.BaseEvent<RunnableObject> {

    }

    /**
     * Signal: stopped
     */
    export interface Stopped extends zrimObject.events.BaseEvent<RunnableObject> {

    }

    /**
     * Signal: stopFailed
     */
    export interface StopFailed extends zrimObject.events.BaseEvent<RunnableObject> {

      /**
       * The error
       */
      error: Error;
    }

    /**
     * Signal: finished
     */
    export interface Finished extends zrimObject.events.BaseEvent<RunnableObject> {

    }

    /**
     * Signal: finishedWithError
     */
    export interface FinishedWithError extends zrimObject.events.BaseEvent<RunnableObject> {

      /**
       * The error
       */
      error: Error;
    }
  }
}

export interface RunnableObject extends InitializableObject {

  /**
   * Ask to start the instance with an options
   * @param options The options
   * @param args
   * @see canStart
   * @see stop
   * @see pause
   * @emits RunnableObject#starting
   * @emits RunnableObject#startFailed
   * @emits RunnableObject#started
   */
  start(options?: runnableObject.StartOptions, ...args: any[]): Promise<void>;

  /**
   * Check if the instance can start
   * @return {boolean} true if can be started, otherwise false
   */
  canStart(): boolean;

  /**
   * Stop the instance. Usually this is to realise the data
   * @param options The options
   * @param args
   * @see canStop
   * @see start
   * @emits RunnableObject#stopping
   * @emits RunnableObject#stopFailed
   * @emits RunnableObject#stopped
   */
  stop(options?: runnableObject.StopOptions, ...args: any[]): Promise<void>;

  /**
   * Check if the instance can be stopped
   * @return {boolean} true if can be stopped, otherwise false
   */
  canStop(): boolean;

  /**
   * Check if the instance is running
   * @return {boolean} true if running, otherwise false
   */
  isRunning(): boolean;

  /**
   * Check if the instance is paused
   * @return {boolean} true if paused, otherwise false
   */
  isPaused(): boolean;

  /**
   * Pause the instance. Usually this is to realise the data
   * @param options The options
   * @param args
   * @see canPause
   * @see resume
   * @emits RunnableObject#paused
   * @emits RunnableObject#pausing
   * @emits RunnableObject#pauseFailed
   */
  pause(options?: runnableObject.PauseOptions, ...args: any[]): Promise<void>;

  /**
   * Check if the instance can be paused
   * @return {boolean} true if can be paused, otherwise false
   */
  canPause(): boolean;

  /**
   * Indicate if the pause mode is authorized by the implementation
   * @return {boolean} true if the pause mode is authorized
   */
  isPauseAuthorized(): boolean;

  /**
   * Pause the instance. Usually this is to realise the data
   * @param options The options
   * @param args
   * @see canResume
   * @see pause
   * @emits RunnableObject#resuming
   * @emits RunnableObject#resumed
   * @emits RunnableObject#resumeFailed
   */
  resume(options?: runnableObject.ResumeOptions, ...args: any[]): Promise<void>;

  /**
   * Check if the instance can be resumed
   * @return {boolean} true if can be resumed, otherwise false
   */
  canResume(): boolean;
}
