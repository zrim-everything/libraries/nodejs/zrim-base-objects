import {ProxyLogger} from 'zrim-proxy-logger';
import {EventEmitter} from "events";

/**
 * An internal flag changed
 *
 * @event ZrimObject#flagChanged
 * @type {object}
 * @property {ZrimObject} _origin The origin instance
 * @property {*} [previousValue] The previous value
 * @property {*} [newValue] The new value
 * @property {string} name The flag name changed
 */
/**
 * A property value changed
 *
 * @event ZrimObject#propertyChanged
 * @type {object}
 * @property {ZrimObject} _origin The origin instance
 * @property {*} [previousValue] The previous value
 * @property {*} [newValue] The new value
 * @property {string} name The property name
 */
/**
 * Ignore Signal
 *
 * @event ZrimObject#ignoreSignalsChanged
 * @type {object}
 * @property {ZrimObject} _origin The origin instance
 * @property {boolean} [previousValue] The previous value
 * @property {boolean} newValue The new value
 */
/**
 * The current state value changed
 *
 * @event ZrimObject#currentStateChanged
 * @type {object}
 * @property {ZrimObject} _origin The origin instance
 * @property {string} [previousValue] The previous value
 * @property {string} newValue The new value
 */
/**
 * A state was put in active list
 *
 * @event ZrimObject#stateActivated
 * @type {object}
 * @property {ZrimObject} _origin The origin instance
 * @property {string} stateName The state name
 */
/**
 * A state was removed from active live
 *
 * @event ZrimObject#stateDeactivated
 * @type {object}
 * @property {ZrimObject} _origin The origin instance
 * @property {string} stateName The state name
 */

export namespace zrimObject {

  export namespace events {

    /**
     * A base event from the base object
     */
    export interface BaseEvent<O = ZrimObject> {
      /**
       * The origin
       */
      _origin: O;
    }

    /**
     * A base event from the base object
     */
    export interface BaseObjectValueChangedEvent<T = any, O = ZrimObject> extends BaseEvent<O> {
      /**
       * The previous value
       */
      previousValue?: T;

      /**
       * The new value
       */
      newValue?: T;
    }

    /**
     * An internal flag changed
     */
    export interface FlagChanged extends BaseObjectValueChangedEvent<any> {

      /**
       * The flag name changed
       */
      name: string;
    }

    /**
     * A property value changed
     */
    export interface PropertyChanged extends BaseObjectValueChangedEvent<any> {

      /**
       * The property name
       */
      name: string;
    }

    /**
     * The ignore signals flag changed
     */
    export interface IgnoreSignalsChanged extends BaseObjectValueChangedEvent<boolean> {

    }

    /**
     * The current state changed
     */
    export interface CurrentStateChanged extends BaseObjectValueChangedEvent<any> {

    }

    /**
     * A state has been activated
     */
    export interface StateActivated extends BaseEvent {
      /**
       * The state name
       */
      stateName: string;
    }

    /**
     * A state has been deactivated
     */
    export interface StateDeactivated extends BaseEvent {
      /**
       * The state name
       */
      stateName: string;
    }
  }
}


/**
 * Special base object
 */
export interface ZrimObject extends EventEmitter {

  /**
   * Internal logger
   */
  readonly logger: ProxyLogger;

  /**
   * The class name
   */
  readonly __className: string;

  /**
   * Indicate if the emit must be blocked
   * @emits ZrimObject#ignoreSignalsChanged
   * @emits ZrimObject#flagChanged
   */
  ignoreSignals: boolean;

  /**
   * Returns the current state name
   * @emits ZrimObject#currentStateChanged
   */
  currentStateName: string;

  /**
   * Shortcut to check if the state is activated, may be the current one or activated one
   * @return true if activated, otherwise false
   * @see isStateActivated
   */
  isReady(): boolean;

  /**
   * Returns the activated state names
   * @return The activated state names
   */
  getActivatedStateNames(): string[];

  /**
   * Test if the given state name is activated or not
   * @param {string} name
   * @return {boolean} true if the state is activated, otherwise false
   */
  isStateActivated(name: string): boolean;
}

