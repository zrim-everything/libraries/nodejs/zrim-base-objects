import {zrimObject, ZrimObject} from "./zrim-object";

/**
 * The initialization started
 *
 * @event InitializableObject#initializing
 * @type {object}
 * @property {InitializableObject} _origin The origin instance
 */
/**
 * The initialize succeed
 *
 * @event InitializableObject#initializationSucceed
 * @type {object}
 * @property {InitializableObject} _origin The origin instance
 */
/**
 * The initialization failed
 *
 * @event InitializableObject#initializationFailed
 * @type {object}
 * @property {InitializableObject} _origin The origin instance
 * @property {Error} error The error
 */
/**
 * The finalization started
 *
 * @event InitializableObject#finalizing
 * @type {object}
 * @property {InitializableObject} _origin The origin instance
 */
/**
 * The finalization succeed
 *
 * @event InitializableObject#finalizationSucceed
 * @type {object}
 * @property {InitializableObject} _origin The origin instance
 */

/**
 * The finalization failed
 *
 * @event InitializableObject#finalizationFailed
 * @type {object}
 * @property {InitializableObject} _origin The origin instance
 * @property {Error} error The error
 */

export namespace initializableObject {

  export interface InitializeOptions {

  }

  export interface FinalizeOptions {

  }

  export namespace events {

    /**
     * Signal: initializing
     */
    export interface Initializing extends zrimObject.events.BaseEvent<InitializableObject> {

    }

    /**
     * Signal: initializationSucceed
     */
    export interface InitializationSucceed extends zrimObject.events.BaseEvent<InitializableObject> {

    }

    /**
     * Signal: initializationFailed
     */
    export interface InitializationFailed extends zrimObject.events.BaseEvent<InitializableObject> {

      /**
       * The error
       */
      error: Error;
    }

    /**
     * Signal: finalizing
     */
    export interface Finalizing extends zrimObject.events.BaseEvent<InitializableObject> {

    }

    /**
     * Signal: finalizationSucceed
     */
    export interface FinalizationSucceed extends zrimObject.events.BaseEvent<InitializableObject> {

    }

    /**
     * Signal: finalizationFailed
     */
    export interface FinalizationFailed extends zrimObject.events.BaseEvent<InitializableObject> {

      /**
       * The error
       */
      error: Error;
    }
  }
}

export interface InitializableObject extends ZrimObject {

  /**
   * Ask to initialize the instance with an options
   * @param options The options
   * @param args The optional arguments
   * @see canInitialize
   * @see finalize
   * @emits InitializableObject#initializing
   * @emits InitializableObject#initializationSucceed
   * @emits InitializableObject#initializationFailed
   */
  initialize(options?: initializableObject.InitializeOptions, ...args: any[]): Promise<void>;

  /**
   * Check if the instance can be initialize
   * @return {boolean} true if can be initialize, otherwise false
   */
  canInitialize(): boolean;

  /**
   * Check if the instance is initialized
   * @return {boolean} true if initialize, otherwise false
   */
  isInitialized(): boolean;

  /**
   * Finalize the instance. Usually this is to realise the data
   * @param options The options
   * @param args The optional arguments
   * @see canFinalize
   * @see initialize
   * @emits InitializableObject#finalizing
   * @emits InitializableObject#finalizationSucceed
   * @emits InitializableObject#finalizationFailed
   */
  finalize(options?: initializableObject.FinalizeOptions, ...args: any[]): Promise<void>;

  /**
   * Check if the instance can be finalize
   * @return {boolean} true if can be finalize, otherwise false
   */
  canFinalize(): boolean;
}
