import {BaseInitializableObject} from "./base-initializable-object";

/**
 * A simple initializable object. It automatically pass from initialized to ready
 */
export class SimpleInitializableObject extends BaseInitializableObject {

  /**
   * @inheritDoc
   */
  protected _getStateNamesForReady(): string[] | undefined {
    return ['initialized'];
  }
}
