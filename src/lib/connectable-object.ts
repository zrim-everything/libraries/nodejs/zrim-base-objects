
/**
 * The connection started
 *
 * @event ConnectableObject#connecting
 * @type {object}
 * @property {ConnectableObject} _origin The origin instance
 */
/**
 * The connection succeed
 *
 * @event ConnectableObject#connected
 * @type {object}
 * @property {ConnectableObject} _origin The origin instance
 */
/**
 * The connection failed
 *
 * @event ConnectableObject#connectionFailed
 * @type {object}
 * @property {ConnectableObject} _origin The origin instance
 * @property {Error} error The error
 */
/**
 * The disconnection started
 *
 * @event ConnectableObject#disconnecting
 * @type {object}
 * @property {ConnectableObject} _origin The origin instance
 */
/**
 * The disconnection succeed
 *
 * @event ConnectableObject#disconnected
 * @type {object}
 * @property {ConnectableObject} _origin The origin instance
 */
/**
 * The disconnection failed
 *
 * @event ConnectableObject#disconnectionFailed
 * @type {object}
 * @property {ConnectableObject} _origin The origin instance
 * @property {Error} error The error
 */
/**
 * The connection was lost
 *
 * @event ConnectableObject#connectionLost
 * @type {object}
 * @property {ConnectableObject} _origin The origin instance
 */


import {zrimObject} from "./zrim-object";
import {InitializableObject} from "./initializable-object";

/**
 * The instance reconnect
 *
 * @event ConnectableObject#reconnected
 * @type {object}
 * @property {ConnectableObject} _origin The origin instance
 */

export namespace connectableObject {

  export interface ConnectOptions {

  }

  export interface DisconnectOptions {

  }

  export namespace events {

    /**
     * Signal: connecting
     */
    export interface Connecting extends zrimObject.events.BaseEvent<ConnectableObject> {

    }

    /**
     * Signal: connected
     */
    export interface Connected extends zrimObject.events.BaseEvent<ConnectableObject> {

    }

    /**
     * Signal: connectionFailed
     */
    export interface ConnectionFailed extends zrimObject.events.BaseEvent<ConnectableObject> {

      /**
       * The error
       */
      error: Error;
    }

    /**
     * Signal: disconnecting
     */
    export interface Disconnecting extends zrimObject.events.BaseEvent<ConnectableObject> {

    }

    /**
     * Signal: disconnected
     */
    export interface Disconnected extends zrimObject.events.BaseEvent<ConnectableObject> {

    }

    /**
     * Signal: disconnectionFailed
     */
    export interface DisconnectionFailed extends zrimObject.events.BaseEvent<ConnectableObject> {

      /**
       * The error
       */
      error: Error;
    }

    /**
     * Signal: connectionLost
     */
    export interface ConnectionLost extends zrimObject.events.BaseEvent<ConnectableObject> {

    }

    /**
     * Signal: reconnected
     */
    export interface Reconnected extends zrimObject.events.BaseEvent<ConnectableObject> {

    }
  }
}

/**
 * A connectable object
 */
export interface ConnectableObject extends InitializableObject {

  /**
   * Ask to connect the instance with an options
   * @param options The options
   * @param args
   * @see canConnect
   * @see disconnect
   * @emits ConnectableObject#connecting
   * @emits ConnectableObject#connected
   * @emits ConnectableObject#connectionFailed
   */
  connect(options?: connectableObject.ConnectOptions, ...args: any[]): Promise<void>;

  /**
   * Check if the instance can be connected
   * @return true if can be connected, otherwise false
   */
  canConnect(): boolean;

  /**
   * Disconnect the instance. Usually this is to realise the data
   * @param options The options
   * @param args
   * @see canDisconnect
   * @see connect
   * @emits ConnectableObject#disconnecting
   * @emits ConnectableObject#disconnected
   * @emits ConnectableObject#disconnectFailed
   */
  disconnect(options?: connectableObject.DisconnectOptions, ...args: any[]): Promise<void>;

  /**
   * Check if the instance can be unconnected
   * @returntrue if can be unconnected, otherwise false
   */
  canDisconnect(): boolean;

  /**
   * Check if the instance is connected
   * @return true if connected, otherwise false
   */
  isConnected(): boolean;
}
