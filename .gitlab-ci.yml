#
# Format version: 0.0.1
#
image: ${ZE_CICD_TOOLBOX_URI}:${ZE_TOOLBOX_VERSION}

variables:
  ZE_GENERATED_VERSION_FILENAME: .VERSION
  ZE_TOOLBOX_VERSION: 0.11.0

stages:
  - requirement
  - version
  - stamp
  - validation
  - build
  - test
  - report
  - publish
  - tag

# ==========================================
#
# Stage: version
#
version:generate:
  stage: version
  script:
    - set -x
    - version=$(version-generate)
    - "echo Version generated: ${version}"
    - echo $version > $ZE_GENERATED_VERSION_FILENAME
  artifacts:
    paths:
      - ${ZE_GENERATED_VERSION_FILENAME}
    expire_in: 1 day
  except:
    - tags
    - triggers

# ==========================================
#
# Stage: stamp
#
stamp:writeVersion:
  stage: stamp
  artifacts:
    paths:
      - package.json
      - package-lock.json
    expire_in: 1 day
  script:
    - set -x
    - npm-set-version
  except:
    - tags
    - triggers

# ==========================================
#
# Stage: validation
#
validation:lint:
  stage: validation
  image: ${ZE_TSLINT_DKR_IMG_URI}:${ZE_TSLINT_DKR_IMG_VERSION}
  script:
    - lint
  except:
    - tags

# ==========================================
#
# Stage: test
#
build:ts:
  stage: build
  script:
    - set -x
    - npm install
    - tsc
    - tsc --declaration
  artifacts:
    paths:
      - dist
  except:
    - tags
    - triggers

# Save build to the s.
build:release-artifacts:
  image: ${ZE_CICD_TOOLBOX_URI}:${ZE_TOOLBOX_VERSION}-docker
  stage: build
  services:
    - docker:dind
  script:
    - set -x
    - mkdir -p ./dist
    - enable-wasabisys
    - ze-aws s3 cp s3://${ZE_ARTIFACTS_S3_BUCKET}/${CI_PROJECT_PATH}/temporary/${CI_COMMIT_SHA}/ ./dist --recursive
  artifacts:
    paths:
      - dist
  only:
    - /^v[0-9]+\.[0-9]+\.[0-9]+$/
  except:
    - branches


# ==========================================
#
# Stage: test
#
.test_template: &test_definition
  stage: test
  services:
    - docker:dind
  script:
    - set -x
    - launch-script-bundle -l $CI_PROJECT_DIR/scripts/test -i -b ${test_suite}
  artifacts:
    expire_in: 30 mins
    when: always
    paths:
      - reports/
  dependencies: []
  except:
    - tags

test:unit:
  <<: *test_definition
  variables:
    test_suite: unit

test:integration:
  <<: *test_definition
  variables:
    test_suite: integration

test:user-defined:
  <<: *test_definition
  variables:
    test_suite: user-defined -r

reports:aggregate:
  stage: report
  variables:
    GIT_STRATEGY: none
  script: "echo 'Aggregating reports...'"
  dependencies:
    - test:unit
    - test:integration
    - test:user-defined
  artifacts:
    name: "${CI_PROJECT_NAME}_test_reports_${CI_COMMIT_REF_NAME}_${CI_COMMIT_SHA}"
    expire_in: 2 weeks
    paths:
      - reports/
  when: always
  except:
    - tags

# ==========================================
#
# Stage: tag
#   - performs automatic tagging for merges in master.
#
tag:ceate:
  stage: tag
  script:
    - set -x
    - version=$(get-project-version)
    - version-tag -r $version
  only:
    - master
  except:
    - tags

# ==========================================
#
# Stage: publish
#
publish:publishSnapshotPackage:
  stage: publish
  script:
    - set -x
    - launch-script-bundle -l $CI_PROJECT_DIR/scripts -i -b pre-publish -r
    - npm-publish --snapshot
    - launch-script-bundle -l $CI_PROJECT_DIR/scripts -i -b ci-complete
  except:
    - master
    - tags
    - triggers

# Save build to the s.
publish:release-artifacts:
  image: ${ZE_CICD_TOOLBOX_URI}:${ZE_TOOLBOX_VERSION}-docker
  stage: publish
  services:
    - docker:dind
  script:
    - set -x
    - enable-wasabisys
    - ze-aws s3 cp ./dist s3://${ZE_ARTIFACTS_S3_BUCKET}/${CI_PROJECT_PATH}/temporary/${CI_COMMIT_SHA}/ --recursive
  only:
    - /^master(?:-[0-9]+)?$/
  except:
    - tags


# Do the publication only if the tag is version major.minor.patch
publish:publishReleasePackage:
  stage: publish
  script:
    - set -x
    - launch-script-bundle -l $CI_PROJECT_DIR/scripts -i -b pre-publish -r
    - npm-set-version --version $CI_COMMIT_TAG
    - npm-publish
    - launch-script-bundle -l $CI_PROJECT_DIR/scripts -i -b ci-complete
  only:
    - /^v[0-9]+\.[0-9]+\.[0-9]+$/
  except:
    - branches
