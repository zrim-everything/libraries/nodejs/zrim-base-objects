# Zrim Base Objects

## Introduction

Contains node.js base objects.

It is always good to have object following the same pattern and be a little generic
to create factory and so.

It provide base object for generic implementation.

## Base Objects

- [BaseObject](documentations/base-object.md)
- [InitializableObject](documentations/initializable-object.md)
- [LoadableObject](documentations/loadable-object.md)
- [ConnectableObject](documentations/connectable-object.md)
- [RunnableObject](documentations/runnable-object.md)

## Watch States
- [SimpleObjectStateWatcher](documentations/object-state-watcher.md)

## Simple Base Object

A simple base is an object which become ready after reaching a special state.

- [SimpleInitializableObject](documentations/simple-initializable-object.md)
- [SimpleLoadableObject](documentations/simple-loadable-object.md)
- [SimpleConnectableObject](documentations/simple-connectable-object.md)
- [SimpleSimpleRunnableObject](documentations/simple-runnable-object.md)

