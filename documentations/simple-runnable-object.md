# Simple Runnable Object

```javascript
require('zrim-base-objects').SimpleRunnableObject
```

Inherits [RunnableObject](runnable-object.md)

## Introduction

Simple object passing to ready after it reach the `initialized` state 
