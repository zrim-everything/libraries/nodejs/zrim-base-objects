# Object State Watcher

```javascript
require('zrim-base-objects').SimpleObjectStateWatcher
```

Inherits [RunnableObject](runnable-object.md)

## Introduction

Object able to watch the synchronization between state.

The object is watching for active state.

## Signals

### synchronized

Emitted when the objects are synchronized

Event *Object*:
- _origin *SimpleObjectStateWatcher*: The emitter

### desynchronized

Emitted when one or more object have different state

Event *Object*:
- _origin *SimpleObjectStateWatcher*: The emitter

## Methods

### initialize

The initialize must receive an object with:
- objects *[BaseObject](base-object.md)[]*: The object to watch. Must be defined,
however it may be empty
- waitingStateName *String*: The state name for which all object must have active
