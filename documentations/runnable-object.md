# Runnable Object

```javascript
require('zrim-base-objects').RunnableObject
```

Inherits [InitializableObject](initializable-object.md)

## Introduction

Allow you to have a base feature of asynchronous running.
Usually it is easier to have a Promise method with a standard name like `start`
to start your object with asynchronous call and `stop` to release the data.

## Signals

### starting

Emitted when the starting

Event *Object*:
- _origin *RunnableObject*: The emitter

### started

Emitted when the instance started

Event *Object*:
- _origin *RunnableObject*: The emitter

### startFailed

Emitted when the instance failed to start

Event *Object*:
- _origin *RunnableObject*: The emitter
- error *Error*: The error

### pausing

Emitted when the instance is pausing

Event *Object*:
- _origin *RunnableObject*: The emitter

### paused

Emitted when the instance paused

Event *Object*:
- _origin *RunnableObject*: The emitter

### pauseFailed

Emitted when the pause failed

Event *Object*:
- _origin *RunnableObject*: The emitter
- error *Error*: The error

### resuming

Emitted when the instance is resuming

Event *Object*:
- _origin *RunnableObject*: The emitter

### resumed

Emitted when the instance resumed

Event *Object*:
- _origin *RunnableObject*: The emitter

### resumeFailed

Emitted when the resume failed

Event *Object*:
- _origin *RunnableObject*: The emitter
- error *Error*: The error

### stopping

Emitted when the instance is stopping

Event *Object*:
- _origin *RunnableObject*: The emitter

### stopped

Emitted when the instance stopped

Event *Object*:
- _origin *RunnableObject*: The emitter

### stopFailed

Emitted when the stop failed

Event *Object*:
- _origin *RunnableObject*: The emitter
- error *Error*: The error

### finished

Emitted when the execution finished with success

Event *Object*:
- _origin *RunnableObject*: The emitter
- result *any*: The result given by the implementation

### finishedWithError

Emitted when the execution finished with success an error

Event *Object*:
- _origin *RunnableObject*: The emitter
- error *Error*: The error

### States

### starting

The object is currently starting

### running

The object is running

### pausing

The object is pausing

### paused

The object is paused

### resuming

The object is resuming

### stopping

The object is stopping

### finished

The object is finished

## Methods

### start

```
start(...args: any[]): Promise<void>
```

Public and generic method to start your object.

This method call internally `_handleStart`.

This method take care of changing the states.

The object must be initialized to be loaded.

### _handleStart

```
_handleStart(...args: any[]): Promise<void>
```

Protected method which should contains the business logic.

Override this method to set your business logic

The last argument is always on object given by the `start`.
It contains a logger which can be use.

### canStart

```
canStart(): boolean
```

Indicate if the object can be started.

### isRunning

```
isRunning(): boolean
```

Tells you if the object is running

### isPaused

```
isPaused(): boolean
```

Tells you if the object is paused

### stop

```
stop(...args: any[]): Promise<void>
```

Public and generic method to stop your object.

This method call internally `_handleStop`.

This method take care of changing the states.

### _handleStop

```
_handleStop(...args: any[]): Promise<void>
```

Protected method which should contains the business logic.

Override this method to set your business logic

The last argument is always on object given by the `stop`.
It contains a logger which can be use.

### canStop

```
canStop(): boolean
```

Indicate if the object can be sopped.

### pause

```
pause(...args: any[]): Promise<void>
```

Public and generic method to pause your object.

This method call internally `_handlePause`.

This method take care of changing the states.

### _handlePause

```
_handlePause(...args: any[]): Promise<void>
```

Protected method which should contains the business logic.

Override this method to set your business logic

The last argument is always on object given by the `pause`.
It contains a logger which can be use.

### canPause

```
canPause(): boolean
```

Indicate if the object can be paused. It look the method `isPauseAuthorized`
to know if the implementation allow the pause mode.

### isPauseAuthorized

```
isPauseAuthorized(): pause
```

Indicate if the implementation allow the pause mode

### resume

```
resume(...args: any[]): Promise<void>
```

Public and generic method to resume your object.

This method call internally `_handleResume`.

This method take care of changing the states.

### _handleResume

```
_handleResume(...args: any[]): Promise<void>
```

Protected method which should contains the business logic.

Override this method to set your business logic

The last argument is always on object given by the `resume`.
It contains a logger which can be use.

### canResume

```
canResume(): boolean
```

Indicate if the object can be resumed.

### _handleRunDone

```
_handleRunDone(result: any): Promise<void>
```

Protected method to be called when the execution finished with success

### _handleExecutionFailed

```
_handleExecutionFailed(error: Error): Promise<void>
```

Protected method to be called when the execution finished with an error





