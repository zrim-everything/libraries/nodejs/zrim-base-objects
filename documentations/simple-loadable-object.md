# Simple Loadable Object

```javascript
require('zrim-base-objects').SimpleLoadableObject
```

Inherits [LoadableObject](loadable-object.md)

## Introduction

Simple object passing to ready after it reach the `loaded` state 
