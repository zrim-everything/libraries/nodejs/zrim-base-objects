# Base Object

```javascript
require('zrim-base-objects').BaseObject
```

Inherits [EventEmitter](https://nodejs.org/api/events.html)

## Introduction

The base object is meant to have default properties and utilities for an object, like
a default logger object, a internal property not enumerable, a state, ...

## Signals

### flagChanged

When an internal flag change its value

Event *Object*:
- _origin *BaseObject*: The emitter
- name *string*: The flag name
- previousValue *any*: The previous value
- newValue *any* : The new value

### propertyChanged

When a property changed. This only works if the object implement the feature

Event *Object*:
- _origin *BaseObject*: The emitter
- name *string*: The property name
- previousValue *any*: The previous value
- newValue *any* : The new value

### ignoreSignalsChanged

Emitted when the ignore signals flag changed

Event *Object*:
- _origin *BaseObject*: The emitter
- previousValue *any*: The previous value
- newValue *any* : The new value

### currentStateChanged

Emitted when the current state changed

Event *Object*:
- _origin *BaseObject*: The emitter
- previousValue *any*: The previous value
- newValue *any* : The new value

### stateActivated

Emitted when a state has been activated

Event *Object*:
- _origin *BaseObject*: The emitter
- stateName *String*: The state name

### stateDeactivated

Emitted when a state has been deactivated

Event *Object*:
- _origin *BaseObject*: The emitter
- stateName *String*: The state name

## States

### none

The default state of the object

### ready

Indicate the object is ready for usage

## Properties

### properties

Type: *Object*

A not enumerable object to contains your protected data. So if there is a `JSON.stringify`
they won't be part of the serialization

### logger

Type: *[BaseProxyLogger](https://gitlab.com/zrim-everything/libraries/nodejs/zrim-proxy-logger/blob/master/documentations/base-proxy-logger.md)*

A proxy logger always available, so you are sure to always have a logger object even if
the logger engine is not set

See `_newProxyLogger`

### __className

Type: *string*

Contains the class name

### ignoreSignals

Type: *boolean*

A flag to disable the emit function, this may be useful in some cases

### currentStateName

Type: *string*

The current state name of the object

## Methods

### isStateActivated

```
isStateActivated(name: string): boolean
```

Ask the object if the given state is activated. The current state may be `a` but the 
state `b` is activated.

### isReady

```
isReady(): boolean
```

Test if the object is ready.
Usually this mean the state `ready` is activated

### getStateNames (static)

```
getStateNames(): string[]
```

Returns the state names the object is able to handle

### getSignalNames (static)

```
getSignalNames(): string[]
```

Returns the signals name the object may emit

### getActivatedStateNames

```
getActivatedStateNames(): string[]
```

Returns the activated state names

### _addActivatedStateName

```
_addActivatedStateName(name: string): void
```

Protected method to add a state name as active

### _removeActivatedStateName

```
_removeActivatedStateName(name: string): void
```

Protected method to remove the state from the active list

### _emit

```
_emit(event: string, ...args: any): any
```

Protected method to emit signals/events. This method ignore the flag `ignoreSignals`

### _emitPropertyChanged

```
_emitPropertyChanged(name: string, newValue: any, previousValue: any): void
```

Protected method to emit the signal of a property changed

This method emit the signal `propertyChanged`.

It is also emitting the signal `<propertyName>Changed`:
- _origin *BaseObject*: The emitter
- previousValue *any*: The previous value
- newValue *any* : The new value

### _emitFlagChanged

```
_emitFlagChanged(name: string, newValue: any, previousValue: any): void
```

Emit a flag value changed.

The signal `flagChanged` is emitted.
This method ignore the flag `ignoreSignals` and always send the event.

### _acceptStateName

```
_acceptStateName(name: string): boolean
```

Protected method used to check if the new state is valid. This allow to protect the object
of invalid states.

### _newProxyLogger

```
_newProxyLogger(...args: any[]): BaseProxyLogger
```

This method is called by the constructor to create a new proxy logger.
The default implementation returns a new
[SimpleGenericProxyLogger](https://gitlab.com/zrim-everything/libraries/nodejs/zrim-proxy-logger/blob/master/documentations/simple-generic-proxy-logger.md)
You can override this method to change the proxy type.

You must ensure to return a class based on [BaseProxyLogger](https://gitlab.com/zrim-everything/libraries/nodejs/zrim-proxy-logger/blob/master/documentations/base-proxy-logger.md).


