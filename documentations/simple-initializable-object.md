# Simple Initializable Object

```javascript
require('zrim-base-objects').SimpleInitializableObject
```

Inherits [InitializableObject](initializable-object.md)

## Introduction

Simple object passing to ready after it reach the `initialized` state 
