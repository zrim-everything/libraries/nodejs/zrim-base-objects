# Loadable Object

```javascript
require('zrim-base-objects').LoadableObject
```

Inherits [InitializableObject](initializable-object.md)

## Introduction

Allow you to have a base feature of asynchronous loading.
Usually it is easier to have a Promise method with a standard name like `load`
to load your object with asynchronous call and `unLoad` to release the data.

## Signals

### loading

Emitted when the load starts

Event *Object*:
- _origin *LoadableObject*: The emitter

### loaded

Emitted when the load finishes with success

Event *Object*:
- _origin *LoadableObject*: The emitter

### loadFailed

Emitted when the load finishes with error

Event *Object*:
- _origin *LoadableObject*: The emitter
- error *Error*: The error

### unLoading

Emitted when the unload starts

Event *Object*:
- _origin *LoadableObject*: The emitter

### unLoaded

Emitted when the unload finishes with success

Event *Object*:
- _origin *LoadableObject*: The emitter

### unLoadFailed

Emitted when the unload finishes with error

Event *Object*:
- _origin *LoadableObject*: The emitter
- error *Error*: The error

### States

### loading

The object is currently under load

### loaded

The object is loaded

### unLoading

The object is currently under unload

## Methods

### load

```
load(...args: any[]): Promise<void>
```

Public and generic method to load your object.

This method call internally `_handleLoad`.

This method take care of changing the states.

The object must be initialized to be loaded.

### _handleLoad

```
_handleLoad(...args: any[]): Promise<void>
```

Protected method which should contains the business logic.

Override this method to set your business logic

The last argument is always on object given by the `load`.
It contains a logger which can be use.

### canLoad

```
canLoad(): boolean
```

Indicate if the object can be loaded.

### isLoaded

```
isLoaded(): boolean
```

Tells you if the object is loaded

### unLoad

```
unLoad(...args: any[]): Promise<void>
```

Public and generic method to unload your object.

This method call internally `_handleUnLoad`.

This method take care of changing the states.

### _handleUnLoad

```
_handleUnLoad(...args: any[]): Promise<void>
```

Protected method which should contains the business logic.

Override this method to set your business logic

The last argument is always on object given by the `unLoad`.
It contains a logger which can be use.

### canUnLoad

```
canUnLoad(): boolean
```

Indicate if the object can be unloaded.
