# Connectable Object

```javascript
require('zrim-base-objects').ConnectableObject
```

Inherits [InitializableObject](initializable-object.md)

## Introduction

Allow you to have a base feature of asynchronous connection.
Usually it is easier to have a Promise method with a standard name like `connect`
to connect your object with asynchronous call and `disconnect` to disconnect the instance.

## Signals

### connecting

Emitted when the connection starts

Event *Object*:
- _origin *ConnectableObject*: The emitter

### connected

Emitted when the connection finishes with success

Event *Object*:
- _origin *ConnectableObject*: The emitter

### connectionFailed

Emitted when the connection finishes with error

Event *Object*:
- _origin *ConnectableObject*: The emitter
- error *Error*: The error

### disconnecting

Emitted when the disconnect starts

Event *Object*:
- _origin *ConnectableObject*: The emitter

### disconnected

Emitted when the disconnection finishes with success

Event *Object*:
- _origin *ConnectableObject*: The emitter

### disconnectionFailed

Emitted when the disconnect finishes with error

Event *Object*:
- _origin *ConnectableObject*: The emitter
- error *Error*: The error

### connectionLost

Emitted when the connection is lost

Event *Object*:
- _origin *ConnectableObject*: The emitter

### reconnected

Emitted when the instance is reconnected

Event *Object*:
- _origin *ConnectableObject*: The emitter

### States

### connecting

The object is currently under connection

### connected

The object is connected

### disconnecting

The object is currently under disconnection

## Methods

### connect

```
connect(...args: any[]): Promise<void>
```

Public and generic method to connect your object.

This method call internally `_handleConnection`.

This method take care of changing the states.

The object must be initialized to be connected.

### _handleConnection

```
_handleConnection(...args: any[]): Promise<void>
```

Protected method which should contains the business logic.

Override this method to set your business logic

The last argument is always on object given by the `connect`.
It contains a logger which can be use.

### canConnect

```
canConnect(): boolean
```

Indicate if the object can be connected.

### isConnected

```
isConnected(): boolean
```

Tells you if the object is connected

### disconnect

```
disconnect(...args: any[]): Promise<void>
```

Public and generic method to disconnect your object.

This method call internally `_handleDisconnection`.

This method take care of changing the states.

### _handleDisconnection

```
_handleDisconnection(...args: any[]): Promise<void>
```

Protected method which should contains the business logic.

Override this method to set your business logic

The last argument is always on object given by the `disconnect`.
It contains a logger which can be use.

### canDisconnect

```
canDisconnect(): boolean
```

Indicate if the object can be disconnected.

### _onConnectionLost

```
_onConnectionLost(...args: any[]): Promise<void>
```

Protected method to be called when the connection is lost.

### _onReconnected

```
_onReconnected(...args: any[]): Promise<void>
```

Protected method to be called when the connection has been reestablished
