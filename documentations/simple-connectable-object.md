# Simple Connectable Object

```javascript
require('zrim-base-objects').SimpleConnectableObject
```

Inherits [ConnectableObject](connectable-object.md)

## Introduction

Simple object passing to ready after it reach the `connected` state 
