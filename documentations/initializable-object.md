# Initializable Object

```javascript
require('zrim-base-objects').InitializableObject
```

Inherits [BaseObject](base-object.md)

## Introduction

Allow you to have a base feature of asynchronous initialization.
Usually it is easier to have a Promise method with a standard name like `initialize`
to initialize your object with asynchronous call and `finalize` to release the data.

## Signals

### initializing

Emitted when the initialization starts

Event *Object*:
- _origin *InitializableObject*: The emitter

### initializationSucceed

Emitted when the initialization finishes with success

Event *Object*:
- _origin *InitializableObject*: The emitter

### initializationFailed

Emitted when the initialization finishes with error

Event *Object*:
- _origin *InitializableObject*: The emitter
- error *Error*: The error

### finalizing

Emitted when the finalization starts

Event *Object*:
- _origin *InitializableObject*: The emitter

### finalizationSucceed

Emitted when the finalization finishes with success

Event *Object*:
- _origin *InitializableObject*: The emitter

### finalizationFailed

Emitted when the finalization finishes with error

Event *Object*:
- _origin *InitializableObject*: The emitter
- error *Error*: The error

### States

### notInitialized

First state of the object, this indicate the object is not initialized

### initializing

The object is currently under initialization

### initialized

The object is initialized

### finalizing

The object is currently under finalization

## Methods

### initialize

```
initialize(...args: any[]): Promise<void>
```

Public and generic method to initialize your object.

This method call internally `_handleInitialization`.

This method take care of changing the states.

### _handleInitialization

```
_handleInitialization(...args: any[]): Promise<void>
```

Protected method which should contains the business logic.

Override this method to set your business logic

The last argument is always on object given by the `initialize`.
It contains a logger which can be use.

### canInitialize

```
canInitialize(): boolean
```

Indicate if the object can be initialize.

### isInitialized

```
isInitialized(): boolean
```

Tells you if the object is initialized

### finalize

```
finalize(...args: any[]): Promise<void>
```

Public and generic method to finalize your object.

This method call internally `_handleFinalization`.

This method take care of changing the states.

### _handleFinalization

```
_handleFinalization(...args: any[]): Promise<void>
```

Protected method which should contains the business logic.

Override this method to set your business logic

The last argument is always on object given by the `finalize`.
It contains a logger which can be use.

### canFinalize

```
canFinalize(): boolean
```

Indicate if the object can be finalize.
